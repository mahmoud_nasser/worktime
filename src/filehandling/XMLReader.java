package filehandling;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import exception.XMLFileException;

public class XMLReader {
	
	private File configFile;
	private Document doc;
	private DocumentBuilderFactory dbFactory;

	
	public XMLReader(File configFile) {
		doc = null;
		dbFactory = DocumentBuilderFactory.newInstance();

		try {
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(configFile);
			doc.getDocumentElement().normalize();
		} catch (SAXException e1) {
			e1.printStackTrace();
			System.err.println("Ett fel som inte kunde hanteras uppstod, programmet avslutas");
			System.exit(1);
			
		} catch (IOException e2) {
			e2.printStackTrace();
			System.exit(1);
			
		} catch (ParserConfigurationException e3) {
			e3.printStackTrace();
			System.exit(1);
		} 
	}

	/**
	 * 
	 * @param tagLevelZero Level zero is the root tag, e.g. "<WorkCalendar>" or "<Settings>"
	 * @param tagLevelOne Level one is the entry level for the calendar, e.g "<WorkDay>" 
	 * @param tagLevelTwo Level two is the field level for calendar, e.g. "<StartTime>" or "<Remarks>"
	 * @return Returns the value searched for at the highest level
	 * @throws XMLFileException The exception that is thrown when a tag at any level is not found.
	 */
	public String getTagValue(String tagLevelZero ,
			String tagLevelOne, String tagLevelTwo) throws XMLFileException {
		
		Element n = null;
		try {
			NodeList nList = doc.getElementsByTagName(tagLevelZero);
			n = (Element) nList.item(0);			
			
		} catch (NullPointerException a) {
			throw new XMLFileException(tagLevelZero);
		}
		try {
			NodeList nList = doc.getElementsByTagName(tagLevelOne);
			n = (Element) nList.item(0);			
			
		} catch (NullPointerException a) {
			throw new XMLFileException(tagLevelOne);
		}
		String value = null;
		try {
			NodeList nList = n.getElementsByTagName(tagLevelTwo);
			n = (Element) nList.item(0);
			nList = n.getChildNodes();
			value = nList.item(0).getNodeValue();
		} catch (NullPointerException a) {
			throw new XMLFileException(tagLevelTwo);
		}
				
		return value;
		
	}
	
}
