package filehandling;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.Map.Entry;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.html.HTML;

import system.Day;
import system.WorkCalendar;
import system.WorkDate;
import UI.MainWindow;
import controllers.Settings;

public class FileHandler {
	final public static String FILE_EXTENSION = "mwc";
	public static String adjustFileNameExtension(String inputedFileName)
	{
		String[] splited = inputedFileName.split("\\.");
		int lastIndex = splited.length==1?0:splited.length-1;
		if(splited[lastIndex].toLowerCase().equals(FILE_EXTENSION.toLowerCase())){
			return inputedFileName;
		}

		int strLen = inputedFileName.length();
		if (inputedFileName.substring(strLen-1).equals(".")){
			return inputedFileName + FILE_EXTENSION;
		}

		return inputedFileName + "." + FILE_EXTENSION;
	}

	public static boolean fileHasCorrectExtension(String fileName){
		String[] splited = fileName.split("\\.");
		if(splited.length<=1){
			return false;
		}
		if(splited[splited.length-1].toLowerCase().equals(FILE_EXTENSION.toLowerCase())){
			return true;
		}
		return false;
	}

	public static JFileChooser newFileChooserInstance(){
		FileNameExtensionFilter filter = new FileNameExtensionFilter("MahnasSoft WorkTime Calendars" , FileHandler.FILE_EXTENSION);
		JFileChooser fc = new JFileChooser();
		fc.addChoosableFileFilter(filter);
		fc.setFileFilter(filter);
		fc.setAcceptAllFileFilterUsed(false);
		fc.setCurrentDirectory(new File(Settings.instance().lastUsedFileChooserDir()));
		return fc;
	}
	/* Serialization methods bellow */
	public static void updateLastUsedDir(JFileChooser fc){
		Settings.instance().lastUsedFileChooserDir(fc.getSelectedFile().getPath());
	}

	public static Object readObjectFromFile(File file) throws IOException, ClassNotFoundException{
		// Read from disk using FileInputStream
		FileInputStream f_in = new FileInputStream(file.toString());
		// Read object using ObjectInputStream
		ObjectInputStream obj_in = new ObjectInputStream (f_in);
		// Read an object
		Object obj = obj_in.readObject();
		obj_in.close();
		return obj;
	}
	public static void saveToFile(File file) throws FileNotFoundException, IOException{
		// Write to disk with FileOutputStream
		FileOutputStream f_out = new FileOutputStream(FileHandler.adjustFileNameExtension(file.toString()));
		// Write object with ObjectOutputStream
		ObjectOutputStream obj_out = new ObjectOutputStream (f_out);
		// Write object out to disk
		obj_out.writeObject(MainWindow.wc);
		obj_out.close();

		MainWindow.MAIN_WINDOW.setNeedSaving(false);
		Settings.instance().currentlyUsedFile(new File(FileHandler.adjustFileNameExtension(file.getPath())));
		MainWindow.MAIN_WINDOW.updateTitle();
	}
	public static void openFromFile(JFileChooser fc) throws ClassNotFoundException, IOException{
		Object obj = FileHandler.readObjectFromFile(fc.getSelectedFile());
		if (obj instanceof WorkCalendar)
		{
			MainWindow.wc = (WorkCalendar) obj;
		}
		MainWindow.MAIN_WINDOW.setNeedSaving(false);
		Settings.instance().currentlyUsedFile(new File(fc.getSelectedFile().getPath()));
		MainWindow.MAIN_WINDOW.updateTitle();
	}

	/* XML Parsing Bellow *//*
	public static void xmlsaveToFile(File file) throws FileNotFoundException, IOException{
		PrintWriter writer = new PrintWriter(file, "UTF-8");
		writer.println("<WorkCalendar>");
		printXMLFromWorkCalendar(writer);
		writer.println("</WorkCalendar>");
		writer.close();
		MainWindow.MAIN_WINDOW.setNeedSaving(false);
		Settings.instance().currentlyUsedFile(new File(FileHandler.adjustFileNameExtension(file.getPath())));
		MainWindow.MAIN_WINDOW.updateTitle();
	}
	private static void printXMLFromWorkCalendar(PrintWriter writer){
		WorkDate wd;
		Day d;
		for(Entry<WorkDate, Day> e : MainWindow.wc.getHashMap()){
			wd =  e.getKey();
			d = e.getValue();

			writer.println("\t<WorkDayEntry>");
			writer.println("\t\t<Date>" + wd.getYear() + "-" + wd.getMonth() + "-" + wd.getDay() + "</Date>");
			writer.println("\t\t<BeginStamp>" + d.timeBegun().getHour()+":"+d.timeBegun().getMinute() + "</BeginStamp>");
			writer.println("\t\t<EndStamp>"+ d.timeEnded().getHour()+":"+d.timeEnded().getMinute() + "</EndStamp>");
			writer.println("\t\t<PauseLength>"+ d.pauseTime().getHour()+":"+d.pauseTime().getMinute() +"</PauseLength>");
			writer.println("\t\t<CheckInFlag>"+(d.getCheckInCardIsScanned()?"True":"False")+"</CheckInFlag>");
			writer.println("\t\t<IllnessFlag>"+(d.getIllness()?"True":"False")+"</IllnessFlag>");
			writer.println("\t\t<Remarks>"+ xmlEscapeText(d.getComment()) +"</Remarks>");
			writer.println("\t</WorkDayEntry>");
		}
	}

	public static String xmlEscapeText(String t) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < t.length(); i++){
			char c = t.charAt(i);
			switch(c){
			case '<': sb.append("&lt;"); break;
			case '>': sb.append("&gt;"); break;
			case '\"': sb.append("&quot;"); break;
			case '&': sb.append("&amp;"); break;
			case '\'': sb.append("&apos;"); break;
			default:
				if(c>0x7e) {
					sb.append("&#"+((int)c)+";");
				}else
					sb.append(c);
			}
		}
		return sb.toString();
	}
*/



}
