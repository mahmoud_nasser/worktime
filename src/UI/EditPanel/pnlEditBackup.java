package UI.EditPanel;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import UI.MainWindow;
import UI.cboMonthChooser;
import UI.cboYearChooser;
import UI.pnlView;
import system.Day;
import system.Time;
import system.WorkDate;
import exception.TimeFormatException;
import exception.TimeStructureException;

public class pnlEditBackup extends pnlView implements TableModelListener{
	//	protected int processedMonth, processedYear;
	//	private JTable t;
	public pnlEditBackup(){
		
	}
	public void pnlEdit(){
		GroupLayout myLayout = new GroupLayout(this);
		setLayout(myLayout);
		//	GroupLayout.SequentialGroup leftToRight = myLayout.createSequentialGroup();
		//	GroupLayout.SequentialGroup topToBottom = myLayout.createSequentialGroup();
		t = new tblTimeEditor();
		//updateTable();
		JScrollPane scroll = new JScrollPane(t);
		JComboBox<String> cboMonthChooser = new cboMonthChooser<String>(this);
		JComboBox<Integer> cboYearChooser = new cboYearChooser<Integer>(this);
		//		.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
		//		.addGroup(myLayout.createSequentialGroup()
		int x = GroupLayout.PREFERRED_SIZE;
		myLayout.setHorizontalGroup(myLayout.createSequentialGroup()
				.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(myLayout.createSequentialGroup()
								)
								.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addGroup(myLayout.createSequentialGroup()
												.addComponent(cboMonthChooser)
												.addComponent(cboYearChooser)
												)
										)
										.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
												.addComponent(scroll)
												)
						)
				);

		myLayout.setVerticalGroup(myLayout.createSequentialGroup()
				.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
						)
						.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(cboMonthChooser,x,x,x)
								.addComponent(cboYearChooser,x,x,x)
								)

								.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addComponent(scroll)
										)				
				);
		//F�rhindrar Resize p� h�jden 
		//myLayout.linkSize(SwingConstants.VERTICAL, cboMonthChooser, cboYearChooser);


		/*

		 GroupLayout.SequentialGroup buttons = myLayout.createSequentialGroup();

		 buttons.addComponent(btn1);
		 buttons.addComponent(btn2);
		 leftToRight.addGroup(buttons);
		 leftToRight.addComponent(t);
		 GroupLayout.ParallelGroup colOne = myLayout.createParallelGroup();
		 colOne.addComponent(btn1);
		 colOne.addComponent(btn2);
		// topToBottom.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.BASELINE));
		 topToBottom.addGroup(colOne);
		 topToBottom.addComponent(t);

		/*
		 http://www.java2s.com/Tutorial/Java/0240__Swing/AFramewithaGroupLayout.htm
		 */

		// myLayout.setHorizontalGroup(leftToRight);
		// myLayout.setVerticalGroup(topToBottom);
		myLayout.setAutoCreateGaps(true);
		myLayout.setAutoCreateContainerGaps(true);
	}


	@Override
	public void tableChanged(TableModelEvent e) {

		int row = e.getFirstRow();
		int column = e.getColumn();
		TableModel model = (TableModel)e.getSource();
		switch (column){
		case 0: //Datum
			return; //g�r inget
		case 5: //Kommentar
			if(((String)model.getValueAt(row, 5)).equals("")){
				return;
			}
			break;
		}
		int day = row+1;
		WorkDate dayBeingProcessed = new WorkDate(processedYear,processedMonth,day);
		if(MainWindow.wc.get(dayBeingProcessed)==null){
			MainWindow.wc.removeEntry(dayBeingProcessed);
		}
		Time start, end, paus;
		Day d;
		try{
			start = StringToTime((String) model.getValueAt(row, 1));
			end = StringToTime((String) model.getValueAt(row, 2));
			paus = StringToTime((String) model.getValueAt(row, 3));
			d = new Day(start, end, paus);
			d.setIllness((Boolean)model.getValueAt(row, 4));
			//System.out.println(model.getValueAt(row, 4)+"\n"+model.getValueAt(row, 5));
			d.setComment((String)model.getValueAt(row, 5));
			MainWindow.wc.put(dayBeingProcessed, d);

		}catch(TimeStructureException e1){
			try{

				System.out.println("Countdown started!");
				Thread.sleep(100);
				for(int x=10;x>=0;x--)
				{
					System.out.println(x);
					Toolkit.getDefaultToolkit().beep();
					Thread.sleep(100);
				}

				e1.printStackTrace();

			}
			catch(InterruptedException e2) { 
				e2.printStackTrace();
			}
			Toolkit.getDefaultToolkit().beep();  
			System.out.println("Structure!");
			e1.printStackTrace();
		}catch(TimeFormatException e1) {
			Toolkit.getDefaultToolkit().beep();
			System.out.println("Format!");
			e1.printStackTrace();
		}catch(NumberFormatException e1){

			Toolkit.getDefaultToolkit().beep();
			System.out.println("Number!");
			e1.printStackTrace();
		}

		updateTable();
	}
	private Time StringToTime(String cellData) throws TimeStructureException, TimeFormatException{
		if (cellData == null){
			return new Time(0,0);
		}
		int strLength = cellData.length(); 
		//Correct formats of time is e.g 15:40, 01:30, 1:30 or 23.12 and 2300 etc. None exceeds 5 characters
		if(strLength>5){ 
			TimeFormatException e = new TimeFormatException();
			e.IPEmessage("\""+cellData+"\" is expected to be in a correct time format, e.g. \"<HOUR><DELIMITER><MINUTE>\"");
			throw e;
		}
		String delimiters[] = {":", "\\.", " "};
		String usedDelimiter="none";
		String timeUnits[]= new String[2];
		for(String delimiter : delimiters){
			timeUnits = cellData.split(delimiter);
			if (timeUnits.length>1){
				usedDelimiter = delimiter;
				break;
			}
		}
		//Making sure that timeUnits[x] isn't longer than 2 chars if a delimier has been used, and begins proccessing it
		//if(timeUnits.length>1&&(!usedDelimiter.equals("none"))&&(timeUnits[0].length()>2 ||timeUnits[1].length()>2)){
		if(timeUnits.length>1&&(!usedDelimiter.equals("none"))&&timeUnits[0].length()<3&&timeUnits[1].length()<3){
			int hour = (int)Integer.parseInt(timeUnits[0]);
			int minute = (int)Integer.parseInt(timeUnits[1]);
			return new Time(hour, minute);	
		}
		//Process time with no delimiters, and with given hours, as "113" (01:13) or "1930" (19:30)
		else if(usedDelimiter.equals("none")&&(timeUnits[0].length()>=3 )){
			int value = (int)Integer.parseInt(timeUnits[0]);
			int hour = (int)value/100;
			int minute = value-(hour*100);
			return new Time(hour, minute);	
		}
		//Process time with no delimiters, and with given hours, as "113" (01:13) or "1930" (19:30)
		else if (usedDelimiter.equals("none")&&timeUnits[0].length()<3){
			int hour = 0;
			int minute;
			if(timeUnits[0].length()>0){
				minute = Integer.parseInt(timeUnits[0]);
			}
			else{
				minute = 0;
			}
			return new Time(hour, minute);
		}
		TimeFormatException e = new TimeFormatException();
		e.IPEmessage("\""+cellData+"\" is expected to be in a correct time format, e.g. \"<HOUR><DELIMITER><MINUTE>\"\nLastExit");
		throw e;
	}
	public void updateTable(){
		if(MainWindow.isTheProgramStartingUp){
			return;
		}
		t.setModel(new tblTimeEditorModell(processedMonth,processedYear));
		t.getTableHeader().setResizingAllowed(false);
		t.getTableHeader().setReorderingAllowed(false);
		t.getModel().addTableModelListener(this);
		//t.setCellSelectionEnabled(true);

	}
}
