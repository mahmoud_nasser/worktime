package UI.EditPanel;

import java.awt.Toolkit;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import UI.MainWindow;
import UI.cboMonthChooser;
import UI.cboYearChooser;
import UI.pnlView;
import system.Day;
import system.Time;
import system.WorkCalendar;
import system.WorkDate;
import exception.TimeFormatException;
import exception.TimeStructureException;

public class pnlEditView extends pnlView implements TableModelListener{
	//	protected int processedMonth, processedYear;
	//	private JTable t;
	
	public pnlEditView(){
		GroupLayout myLayout = new GroupLayout(this);
		setLayout(myLayout);
		//	GroupLayout.SequentialGroup leftToRight = myLayout.createSequentialGroup();
		//	GroupLayout.SequentialGroup topToBottom = myLayout.createSequentialGroup();
		t = new tblTimeEditor();
		//updateTable();
		JScrollPane scroll = new JScrollPane(t);
		JComboBox<String> cboMonthChooser = new cboMonthChooser<String>(this);
		JComboBox<Integer> cboYearChooser = new cboYearChooser<Integer>(this);
		//		.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
		//		.addGroup(myLayout.createSequentialGroup()
		int x = GroupLayout.PREFERRED_SIZE;
		myLayout.setHorizontalGroup(myLayout.createSequentialGroup()
				.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
						/*.addGroup(myLayout.createSequentialGroup()
						 * Buttons where located here
								)*/
								.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addGroup(myLayout.createSequentialGroup()
												.addComponent(cboMonthChooser)
												.addComponent(cboYearChooser)
												)
										)
										.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
												.addComponent(scroll)
												)
						)
				);

		myLayout.setVerticalGroup(myLayout.createSequentialGroup()
				/*.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
				 * Buttons where located here
						)*/
						.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(cboMonthChooser,x,x,x)
								.addComponent(cboYearChooser,x,x,x)
								)

								.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addComponent(scroll)
										)				
				);
		//F�rhindrar Resize p� h�jden 
		//myLayout.linkSize(SwingConstants.VERTICAL, cboMonthChooser, cboYearChooser);


		/*

		 GroupLayout.SequentialGroup buttons = myLayout.createSequentialGroup();

		 buttons.addComponent(btn1);
		 buttons.addComponent(btn2);
		 leftToRight.addGroup(buttons);
		 leftToRight.addComponent(t);
		 GroupLayout.ParallelGroup colOne = myLayout.createParallelGroup();
		 colOne.addComponent(btn1);
		 colOne.addComponent(btn2);
		// topToBottom.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.BASELINE));
		 topToBottom.addGroup(colOne);
		 topToBottom.addComponent(t);

		/*
		 http://www.java2s.com/Tutorial/Java/0240__Swing/AFramewithaGroupLayout.htm
		 */

		// myLayout.setHorizontalGroup(leftToRight);
		// myLayout.setVerticalGroup(topToBottom);
		myLayout.setAutoCreateGaps(true);
		myLayout.setAutoCreateContainerGaps(true);
	}


	@Override
	public void tableChanged(TableModelEvent e) {

		WorkCalendar wc = MainWindow.wc;
		int row = e.getFirstRow();
		int column = e.getColumn();
		TableModel model = (TableModel)e.getSource();
		int day = row+1;
		WorkDate dayBeingProcessed = new WorkDate(processedYear,processedMonth,day);
		
		if(dayBeingProcessed==null){
			System.err.println("Vid identifiering av readigerad rad/dag");
			return;
		}
		Day d = new Day();		//Variabel som ska in i Kalendern
		Day dBeforeChange = wc.get(dayBeingProcessed); //Existerande v�rdet i Workcal.
		//Tempor�ra variabler f�r Begin, End & Paus
		Time start = Time.MIDNIGHT;
		Time end = Time.MIDNIGHT;
		Time pause = Time.MIDNIGHT;
		
		//Try h�r uppe, eftersom inget ska g� igenom om vi har felaktig inmatning
		try{
		switch (column){
		case 0: //Date
			return; //Do nothing
		case 1: //Start Time
			start = StringToTime((String) model.getValueAt(row, 1));
			if(dBeforeChange==null){
				d = new Day(start, end, pause);
			}
			else{
				end = dBeforeChange.timeEnded();
				pause = dBeforeChange.pauseTime();
				d = new Day(start, end, pause);
				d.setCheckInCardIsScanned(dBeforeChange.getCheckInCardIsScanned());
				d.setComment(dBeforeChange.getComment());
				d.setIllness(dBeforeChange.getIllness());
			}
			break;
			
		case 2: //End time
			end = StringToTime((String) model.getValueAt(row, 2));
			if(dBeforeChange==null){
				d = new Day(start, end, pause);
			}
			else{
				start = dBeforeChange.timeBegun();
				pause = dBeforeChange.pauseTime();
				d = new Day(start, end, pause);
				d.setCheckInCardIsScanned(dBeforeChange.getCheckInCardIsScanned());
				d.setComment(dBeforeChange.getComment());
				d.setIllness(dBeforeChange.getIllness());
			}
			break;
		case 3: //Pause
			pause = StringToTime((String) model.getValueAt(row, 3));
			if(dBeforeChange==null){
				d = new Day(start, end, pause);
			}
			else{
				start = dBeforeChange.timeBegun();
				end = dBeforeChange.timeEnded();
				d = new Day(start, end, pause);
				d.setCheckInCardIsScanned(dBeforeChange.getCheckInCardIsScanned());
				d.setComment(dBeforeChange.getComment());
				d.setIllness(dBeforeChange.getIllness());
			}
			break;
		case 4: //Ill
			boolean illness = (Boolean)model.getValueAt(row, 4);
			if(dBeforeChange==null){
				d = new Day(start, end, pause);
				d.setIllness(illness);
			}
			else{
				start = dBeforeChange.timeBegun();
				end = dBeforeChange.timeEnded();
				pause = dBeforeChange.pauseTime();
				d = new Day(start, end, pause);
				d.setCheckInCardIsScanned(dBeforeChange.getCheckInCardIsScanned());
				d.setComment(dBeforeChange.getComment());
				d.setIllness(illness);
			}
			
			break;
		case 5: //Comment
			String comment = (String)model.getValueAt(row, 5);
			if(dBeforeChange==null){
				if(comment == null || comment.equals("")){
					return;
				}
				d = new Day(start, end, pause);
				d.setComment(comment);
			}
			else{
				start = dBeforeChange.timeBegun();
				end = dBeforeChange.timeEnded();
				pause = dBeforeChange.pauseTime();
				d = new Day(start, end, pause);
				d.setCheckInCardIsScanned(dBeforeChange.getCheckInCardIsScanned());
				d.setComment(comment);
				d.setIllness(dBeforeChange.getIllness());
			}
			break;
		}

		MainWindow.wc.removeEntry(dayBeingProcessed);
		MainWindow.wc.put(dayBeingProcessed, d);
		
		

		}catch(TimeStructureException e1){
			Toolkit.getDefaultToolkit().beep();  
			System.out.println("Structure!");
		}catch(TimeFormatException e1) {
			Toolkit.getDefaultToolkit().beep();
			System.out.println("Format!");
		}catch(NumberFormatException e1){
			Toolkit.getDefaultToolkit().beep();
			System.out.println("Number!");
		}

		updateTable();
	}
	private Time StringToTime(String cellData) throws TimeStructureException, TimeFormatException{
		if (cellData == null){
			return new Time(0,0);
		}
		int strLength = cellData.length(); 
		//Correct formats of time is e.g 15:40, 01:30, 1:30 or 23.12 and 2300 etc. None exceeds 5 characters
		if(strLength>5){ 
			TimeFormatException e = new TimeFormatException();
			e.IPEmessage("\""+cellData+"\" is expected to be in a correct time format, e.g. \"<HOUR><DELIMITER><MINUTE>\"");
			throw e;
		}
		String delimiters[] = {":", "\\.", " "};
		String usedDelimiter="none";
		String timeUnits[]= new String[2];
		for(String delimiter : delimiters){
			timeUnits = cellData.split(delimiter);
			if (timeUnits.length>1){
				usedDelimiter = delimiter;
				break;
			}
		}
		//Making sure that timeUnits[x] isn't longer than 2 chars if a delimier has been used, and begins proccessing it
		//if(timeUnits.length>1&&(!usedDelimiter.equals("none"))&&(timeUnits[0].length()>2 ||timeUnits[1].length()>2)){
		if(timeUnits.length>1&&(!usedDelimiter.equals("none"))&&timeUnits[0].length()<3&&timeUnits[1].length()<3){
			int hour = (int)Integer.parseInt(timeUnits[0]);
			int minute = (int)Integer.parseInt(timeUnits[1]);
			return new Time(hour, minute);	
		}
		//Process time with no delimiters, and with given hours, as "113" (01:13) or "1930" (19:30)
		else if(usedDelimiter.equals("none")&&(timeUnits[0].length()>=3 )){
			int value = (int)Integer.parseInt(timeUnits[0]);
			int hour = (int)value/100;
			int minute = value-(hour*100);
			return new Time(hour, minute);	
		}
		//Process time with no delimiters, and with no given hours, as "13" (00:13) or "5" (00:05)
		else if (usedDelimiter.equals("none")&&timeUnits[0].length()<3){
			int hour = 0;
			int minute;
			if(timeUnits[0].length()>0){
				minute = Integer.parseInt(timeUnits[0]);
			}
			else{
				minute = 0;
			}
			return new Time(hour, minute);
		}
		TimeFormatException e = new TimeFormatException();
		e.IPEmessage("\""+cellData+"\" is expected to be in a correct time format, e.g. \"<HOUR><DELIMITER><MINUTE>\"\nLastExit");
		throw e;
	}
	public void updateTable(){
		if(MainWindow.isTheProgramStartingUp){
			return;
		}
		t.setModel(new tblTimeEditorModell(processedMonth,processedYear));
		t.getTableHeader().setResizingAllowed(false);
		t.getTableHeader().setReorderingAllowed(false);
		t.getModel().addTableModelListener(this);
		//t.setCellSelectionEnabled(true);

	}
}
