package UI.EditPanel;

import javax.swing.table.AbstractTableModel;

import UI.MainWindow;
import controllers.LanguageStringData;
import system.WorkCalendar;
import system.WorkDate;

public class tblTimeEditorModell  extends AbstractTableModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String[] columnNames;
	private Object[][] data;
	private WorkCalendar wc;
	public int columnWidth[];

	tblTimeEditorModell(int paymentMonth, int paymentYear){
		this.wc = MainWindow.wc;
	//	WorkDate currentPrintDate = WorkDate.monthPaymentBegin(paymentYear, paymentMonth); //Begin printing PaymentMont (16/Month-1/Year to 15/Month/Year
	//	int monthLength = WorkDate.getLengthOfMonth(paymentMonth-1, paymentYear);
		
		WorkDate currentPrintDate = new WorkDate(paymentYear, paymentMonth, 1);
		int monthLength = WorkDate.getLengthOfMonth(paymentMonth, paymentYear);
		
		WorkDate monthEnd = new WorkDate(paymentYear, paymentMonth, monthLength );
		int columnCount = 6;
		columnNames = new String[columnCount];
		columnNames[0] = LanguageStringData.G_DATE();
		columnNames[1] = LanguageStringData.E_BEGUN();
		columnNames[2] = LanguageStringData.E_ENDED();
		columnNames[3] = LanguageStringData.G_BREAK();
		columnNames[4] = LanguageStringData.G_SICK();
	//	columnNames[5] = "Inscannad";
		columnNames[5] = LanguageStringData.G_PERSONAL_REMARKS();
	


		data = new Object[monthLength][7]; //[Rader][Kolumner]
		//Calendar iterCal = new Calendar(wc.,m,y).getInstance
		for(int tableIndex=0; tableIndex<monthLength; tableIndex++){//For each date in between choosen month, eg Januari 2012, get 16 Dec 2011 to 15 Jan 2012
			data[tableIndex][0] = currentPrintDate.toString();
			//System.out.println("tableIndex: "+tableIndex+"\nDate: "+currentPrintDate+"\n\n");
			if(wc.get(currentPrintDate)==null){
				currentPrintDate = currentPrintDate.increaseDate();
				data[tableIndex][4] = false;
			//	data[tableIndex][5] = true;				
				continue;
			}
			data[tableIndex][1] = wc.get(currentPrintDate).timeBegun().toString();
			data[tableIndex][2] = wc.get(currentPrintDate).timeEnded().toString();
			data[tableIndex][3] = wc.get(currentPrintDate).pauseTime().toString();
			data[tableIndex][4] = wc.get(currentPrintDate).getIllness();
		//	data[tableIndex][5] = wc.get(currentPrintDate).getCheckInCardIsScanned();
			data[tableIndex][5] = wc.get(currentPrintDate).getComment();
			if(currentPrintDate.hashCode() == monthEnd.hashCode()){
				break;
			}
			currentPrintDate = currentPrintDate.increaseDate();
			
		}

	}
	/*
	tblEditTableModell(){
		columnNames = new String[2];
		columnNames[0] = "B�rjade";
		columnNames[1] = "Slutade";


		data = new Object[4][2]; //[Rader][Kolumner]
		data[0][0] = "Hej";
		data[0][1] = new Boolean(true);
		data[1][0] = "Haj";
		data[1][1] = new Boolean(false);
		data[2][0] = null;
		data[2][1] = null;
		data[3][0] = "Seh�r";
		data[3][1] = new Boolean(true);
	}
	*/
	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return data.length;
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Object getValueAt(int row, int col) {
		return data[row][col];
	}

	public Class getColumnClass(int c) {
		//workaround
		Object value=this.getValueAt(0,c);  
		return (value==null?Object.class:value.getClass()); 
		//*
		//return getValueAt(0, c).getClass(); //original
	}

	/*
	 * Don't need to implement this method unless your table's
	 * editable.
	 */
	public boolean isCellEditable(int row, int col) {
		//Note that the data/cell address is constant,
		//no matter where the cell appears onscreen.
		if (col < 1) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Don't need to implement this method unless your table's
	 * data can change.
	 */
	@Override
	public void setValueAt(Object value, int row, int col) {
		data[row][col] = value;		
		fireTableCellUpdated(row, col);
		MainWindow.MAIN_WINDOW.setNeedSaving(true);

	}
	
	
	
	}