package UI.Menubar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;

import controllers.LanguageStringData;
import controllers.Settings;
import UI.MainWindow;

public class itmLang extends JCheckBoxMenuItem implements ActionListener{
	int langID;
	public itmLang(int languageID){		
		super(LanguageStringData.M_E_L_ITM(languageID));
		langID = languageID;
		if(langID == Settings.instance().getLanguage()){
			super.setState(true);
		}
		else{
			super.setState(false);
		}
		init_Component();
	}

	private void init_Component(){
		this.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Settings.instance().setLanguage(langID);
		if (Settings.instance().getLanguage() == langID){
			setState(true);
		}
		else{
			setState(false);
		}
	}

}
