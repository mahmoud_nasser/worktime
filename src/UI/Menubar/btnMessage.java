package UI.Menubar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import UI.MainWindow;

public class btnMessage extends JButton implements ActionListener{

	private String message;
	public btnMessage(String buttonText, String message){
		super(buttonText);
		init_Component(message);
	}
	public btnMessage(String message){
		init_Component(message);
	}
	
	private void init_Component(String message){
		this.addActionListener(this);
		this.message = message;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JOptionPane.showMessageDialog(MainWindow.MAIN_WINDOW, message);
		
	}

}
