package UI.Menubar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import controllers.LanguageStringData;
import controllers.Settings;
import UI.MainWindow;
import system.WorkCalendar;

public class itmNew extends JMenuItem implements ActionListener{

	public itmNew(String buttonText){
		super(buttonText);
		init_Component();
	}
	public itmNew(){
		init_Component();
	}

	private void init_Component(){
		this.setMnemonic('N');
		this.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(MainWindow.MAIN_WINDOW.needSaving){
			int userOption = JOptionPane.showConfirmDialog(MainWindow.MAIN_WINDOW, LanguageStringData.F_UNSAVED_CHANGES(), LanguageStringData.F_UNSAVED_CHANGES_TITLE(), JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);	
			if (userOption == JOptionPane.NO_OPTION){
				return;
			}
		}
		Settings.instance().currentlyUsedFile(null);
		MainWindow.wc = new WorkCalendar();
		MainWindow.MAIN_WINDOW.setNeedSaving(false);
		MainWindow.MAIN_WINDOW.updateTitle();
		MainWindow.MAIN_WINDOW.updatePanelViews();

	}

}
