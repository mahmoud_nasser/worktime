package UI.Menubar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import controllers.LanguageStringData;
import controllers.Settings;
import UI.MainWindow;
import filehandling.FileHandler;
import system.WorkCalendar;

public class itmOpen extends JMenuItem implements ActionListener{
	public itmOpen(String buttonText){
		super(buttonText);
		init_Component();
	}
	public itmOpen(){
		init_Component();
	}

	private void init_Component(){
		this.setMnemonic('O');
		this.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(MainWindow.MAIN_WINDOW.needSaving){
			int userOption = JOptionPane.showConfirmDialog(MainWindow.MAIN_WINDOW, LanguageStringData.F_UNSAVED_CHANGES(), LanguageStringData.F_QUIT_UNSAVED_CHANGES_TITLE(), JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);	
			if (userOption == JOptionPane.NO_OPTION){
				return;
			}
		}
		JFileChooser fc = FileHandler.newFileChooserInstance();
		int returnVal = fc.showOpenDialog(MainWindow.MAIN_WINDOW);
		if(!(returnVal == JFileChooser.APPROVE_OPTION)){
			System.err.println("FILE OPEN ERROR");
			return;
		}
		if(!FileHandler.fileHasCorrectExtension(fc.getSelectedFile().toString())){
			JOptionPane.showMessageDialog(fc, LanguageStringData.F_OPEN_UNSUPPORTED_FILE(), LanguageStringData.F_OPEN_UNSUPPORTED_FILE_TITLE(), JOptionPane.ERROR_MESSAGE);
			return;
		}
		FileHandler.updateLastUsedDir(fc);
		try {			
			FileHandler.openFromFile(fc);
		}
		catch(Exception e1){

		}
		MainWindow.MAIN_WINDOW.updatePanelViews();
		MainWindow.MAIN_WINDOW.updateTitle();

	}

}
