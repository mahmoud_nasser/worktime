package UI.Menubar;

import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import controllers.LanguageStringData;
import controllers.Settings;

public class Menubar extends JMenuBar{
	JMenu menuFile;
	JMenu menuEdit;
	JMenu menuLang;
	JMenu menuPayroll;
	JMenu menuHelp;
	JMenuItem[] fileMenItems;
	JMenuItem[] editMenItems;
	JMenuItem[] langMenItems;
	JMenuItem[] payrollMenItems;
	public Menubar(){
		super();
		fileMenItems = new JMenuItem[5];
		fileMenItems[0] = new itmNew(LanguageStringData.M_E_NEW());
		fileMenItems[1] = new itmOpen(LanguageStringData.M_E_OPEN());
		fileMenItems[2] = new itmSave(LanguageStringData.M_E_SAVE(), false);
		fileMenItems[3] = new itmSave(LanguageStringData.M_E_SAVE_AS(), true);
		fileMenItems[4] = new itmClose(LanguageStringData.M_E_QUIT());
		menuFile = new JMenu(LanguageStringData.M_FILE());
		for(int i=0; i<fileMenItems.length; i++){
			menuFile.add(fileMenItems[i]);
			if(i==1||i==3){
				menuFile.addSeparator();
			}
		}
		menuFile.setMnemonic('F');
		add(menuFile);
		
		
		langMenItems = new JMenuItem[3];
		langMenItems[0] = new itmLang(Settings.LANG_ENG);
		langMenItems[1] = new itmLang(Settings.LANG_SWE);
		langMenItems[2] = new itmLang(Settings.LANG_DAN);
		menuLang = new JMenu(LanguageStringData.M_E_LANGUAGE());
		for(int i=0; i<langMenItems.length; i++){
			menuLang.add(langMenItems[i]);
		}		
		menuLang.setMnemonic('L');
		//add(menuLang);
		ButtonGroup payrollBtnGrp = new ButtonGroup();
		payrollMenItems = new JMenuItem[2];
		payrollMenItems[0] = new itmPayrollSystem(LanguageStringData.M_E_PS_SWEDISH(), Settings.PAYROLL_SYSTEM_SWEDISH);
		payrollMenItems[1] = new itmPayrollSystem(LanguageStringData.M_E_PS_DANISH(), Settings.PAYROLL_SYSTEM_DANISH);
		menuPayroll = new JMenu(LanguageStringData.M_E_PAYROLL_SYSTEM());
		for(int i=0; i<payrollMenItems.length; i++){
			menuPayroll.add(payrollMenItems[i]);
			payrollBtnGrp.add(payrollMenItems[i]);
		}		
		payrollMenItems[Settings.instance().currentPayrollSystem].setSelected(true);
		menuPayroll.setMnemonic('S');
		
		menuEdit = new JMenu(LanguageStringData.M_EDIT());
		menuEdit.add(menuLang);
		menuEdit.add(menuPayroll);
		menuEdit.setMnemonic('E');
		add(menuEdit);
		
	
		
		
		
	
		
		menuHelp = new JMenu(LanguageStringData.M_HELP());
		JMenuItem about = new itmMessage(LanguageStringData.M_H_ABOUT()+" WorkTime", itmMessage.ABOUT_MESSAGE);
		about.setMnemonic('A');
		JMenuItem issues = new itmMessage(LanguageStringData.M_H_ISSUES(), itmMessage.KNOWN_ISSUES);
		about.setMnemonic('I');
		JMenuItem guide = new itmMessage(LanguageStringData.M_H_GETTING_STARTED(), itmMessage.GETTING_STARTED);
		about.setMnemonic('G');
		
		menuHelp.add(guide);
		menuHelp.add(issues);
		menuHelp.add(about);
		menuHelp.setMnemonic('H');
		add(menuHelp);
		
		


	}

}
