package UI.Menubar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import system.WorkCalendar;
import UI.Guide;
import UI.MainWindow;
import controllers.LanguageStringData;

public class itmMessage extends JMenuItem implements ActionListener{
	final private static String aboutMessage = "MahNasSoft WorkTime \nVersion "+WorkCalendar.SYSTEM_VERSION+"\n\n\nMahmoud Nasser  � 2012-2015\nMahNas Software  � 2012-2015";
	final private static String unimplementedMessage = "You are trying to access an unimplemented function or feature.";
	final private static String issuesMessage = "ISSUES\n\n1) Once a day is created there is no way to delete it.\n"
			+ "However one can have all timestamps at '00:00' and everything will function normally...";
	final public static int UNIMPLEMENTED_FUNCTION = 0;
	final public static int ABOUT_MESSAGE = 1;
	final public static int GETTING_STARTED = 2;
	final public static int KNOWN_ISSUES = 3;
	private int message;
	public itmMessage(String itemText, int message){
		super(itemText+(message==0?"*":""));
		this.message = message;
		this.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(message == UNIMPLEMENTED_FUNCTION){
			JOptionPane.showMessageDialog(MainWindow.MAIN_WINDOW, unimplementedMessage, "Unimplemented Function", JOptionPane.ERROR_MESSAGE);	
		}
		else if(message == ABOUT_MESSAGE){
			JOptionPane.showMessageDialog(MainWindow.MAIN_WINDOW, aboutMessage, LanguageStringData.M_H_ABOUT()+" MNS WorkTime", JOptionPane.INFORMATION_MESSAGE);
		}
		else if(message == GETTING_STARTED){
			new Guide();
		}
		else if(message == KNOWN_ISSUES){
			JOptionPane.showMessageDialog(MainWindow.MAIN_WINDOW, issuesMessage, LanguageStringData.M_H_ISSUES()+" MNS WorkTime", JOptionPane.PLAIN_MESSAGE);
		}

	}

}
