package UI.Menubar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

import UI.MainWindow;

public class itmClose extends JMenuItem implements ActionListener{
	public itmClose(String buttonText){
		super(buttonText);
		init_Component();
	}
	public itmClose(){
		init_Component();
	}
	
	private void init_Component(){
		this.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		MainWindow.MAIN_WINDOW.ExitProgram();
	}

}
