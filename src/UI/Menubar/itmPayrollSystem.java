package UI.Menubar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JRadioButtonMenuItem;

import UI.SummaryPanel.pnlMonthlyView;
import controllers.Settings;

public class itmPayrollSystem extends JRadioButtonMenuItem implements ActionListener{
	int countryID;
	public itmPayrollSystem(String country, int countryID){		
		super(country);
		this.countryID = countryID;
		init_Component();
	}

	private void init_Component(){
		this.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Settings.instance().currentPayrollSystem = countryID;
		pnlMonthlyView.currInstance.updateTable();
	}

}
