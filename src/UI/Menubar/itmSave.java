package UI.Menubar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import controllers.LanguageStringData;
import controllers.Settings;
import UI.MainWindow;
import filehandling.FileHandler;
import system.WorkCalendar;

public class itmSave extends JMenuItem implements ActionListener{
	private boolean isSaveAs;
	public itmSave(String buttonText, boolean isSaveAs){
		super(buttonText);
		init_Component();
		this.isSaveAs = isSaveAs;
	}
	public itmSave(){
		init_Component();
	}

	private void init_Component(){
		this.addActionListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		save(isSaveAs);		
	}
	public static boolean save(boolean isSaveAs){
		File targetFile = null;
		if(isSaveAs || Settings.instance().currentlyUsedFile()==null){
			JFileChooser fc = FileHandler.newFileChooserInstance();
			if(Settings.instance().currentlyUsedFile()==null){
				fc.setSelectedFile(new File("My WorkTime Calendar.mwc"));
			}
			int overWriteDialog = JOptionPane.YES_OPTION;
			do{
				int returnVal = fc.showSaveDialog(MainWindow.MAIN_WINDOW);
				if(!(returnVal == JFileChooser.APPROVE_OPTION)){
					System.err.println("Otill�ten fil �ppnad! (btnSave.java: #42)");
					return false;
				}
				File f = new File(FileHandler.adjustFileNameExtension(fc.getSelectedFile().getPath()));
				if(f.exists()){
					overWriteDialog = JOptionPane.showConfirmDialog(fc, LanguageStringData.F_SAVE_OVERWRITE(), LanguageStringData.F_SAVE_OVERWRITE_TITLE(), JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
					if(overWriteDialog == JOptionPane.CANCEL_OPTION){
						return false;
					}
				}
			} while(overWriteDialog != JOptionPane.YES_OPTION);
			FileHandler.updateLastUsedDir(fc);
			targetFile = fc.getSelectedFile();
		}
		else{
			targetFile = Settings.instance().currentlyUsedFile();
		}
		try {
			FileHandler.saveToFile(targetFile);
			return true;
		} catch (IOException e1) {
			e1.printStackTrace();
			JOptionPane.showMessageDialog(MainWindow.MAIN_WINDOW, LanguageStringData.F_SAVE_ERROR(), LanguageStringData.F_SAVE_ERROR_TITLE(),JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}

}
