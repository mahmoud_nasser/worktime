package UI;

import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import UI.EditPanel.pnlEditView;
import UI.SummaryPanel.pnlMonthlyView;
import controllers.LanguageStringData;

public class TabbedPane extends JTabbedPane implements ChangeListener{
	private pnlView[] panels;
	private final int P_EDITOR = 0;
	private final int P_MONTHSUMMARIZER = 1;
	public TabbedPane(){
		super();
		panels = new pnlView[2];
		panels[P_EDITOR] = new pnlEditView();
		panels[P_MONTHSUMMARIZER] = new pnlMonthlyView();
		this.addTab(LanguageStringData.E_TITLE(), null, panels[P_EDITOR], LanguageStringData.E_TOOLTIP());
		this.addTab(LanguageStringData.O_TITLE(), null, panels[P_MONTHSUMMARIZER], LanguageStringData.O_TOOLTIP());
		this.addChangeListener(this);
		MainWindow.isTheProgramStartingUp = false;
		this.panels[getSelectedIndex()].updateTable();
	}
	@Override
	public void stateChanged(ChangeEvent e) {
		this.panels[getSelectedIndex()].updateTable();		
	}
	public void updatePanelViews(){
		for(pnlView p:panels){
			p.updateTable();
		}
	}

}
