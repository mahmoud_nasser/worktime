package UI;

import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Timestamp;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import controllers.LanguageStringData;
import controllers.Settings;
import UI.Menubar.Menubar;
import UI.Menubar.itmSave;
import system.WorkCalendar;
public class MainWindow extends JFrame{
	private TabbedPane tabs;
	public boolean needSaving;
	
	public static MainWindow MAIN_WINDOW;
	public static boolean isTheProgramStartingUp;
	public static WorkCalendar wc = new WorkCalendar();

	public static void main(String[]args){
		/*
		//Printar alla errors till en fil
		File errorFile = new File("errors_log.txt");
		String oldContent;
		try {
			FileOutputStream fos;
			fos = new FileOutputStream(errorFile, true);
			PrintStream ps = new PrintStream(fos);
			System.setErr(ps);
			//Printa host - OS och Timestamp
			Scanner fileRead = new Scanner(errorFile);
			ps.println("------------------------------");
			java.util.Date date= new java.util.Date();
			ps.println(System.getProperty("os.name"));
			ps.println((new Timestamp(date.getTime())).toString());
			ps.println("---------------");

		} catch (FileNotFoundException e1) {
			JOptionPane.showMessageDialog(null, "Warning!\nErrors and debuging information could not be redirected to file", "System.err redirection error",JOptionPane.INFORMATION_MESSAGE);
		};
		/*********/

		//S�tts till false i konstruktorn f�r "TabbedPane"
		isTheProgramStartingUp = true;
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} 
		catch(Exception e) {
			System.err.println("Look and feel error!");
		}
		/* H�rdkodad inl�gg *
		try {
			wc.put(new WorkDate(2012,4,5), new Day(new Time(8,40), new Time(16,20)));
		} catch (TimeStructureException e) {
			e.printStackTrace();
		}
		//*/
		MainWindow.MAIN_WINDOW = new MainWindow();
		isTheProgramStartingUp = false;

	}
	public MainWindow(){
		needSaving = false;
		Settings.instance().loadSettings();
		tabs = new TabbedPane();
		this.add(tabs);
		JMenuBar menu = new Menubar();
		setJMenuBar(menu);
		this.pack();
		this.setBounds(150, 50, 800, 600);
		this.setVisible(true);
		this.updateTitle();
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		//this.setIconImage(new ImageIcon(getClass().getResource("/resource/icon.png")).getImage());
		addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)
			{
				int userOption=31101; //Error...
				if(needSaving){
					userOption = JOptionPane.showConfirmDialog(MAIN_WINDOW, LanguageStringData.F_QUIT_UNSAVED_CHANGES(), LanguageStringData.F_QUIT_UNSAVED_CHANGES_TITLE(), JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);

					if(userOption == JOptionPane.YES_OPTION){
						boolean isSavedCorrectly=false;

						isSavedCorrectly = itmSave.save(false);
						if(isSavedCorrectly){
							exitAndCloseProgram();
						}
					}
					else if(userOption == 31101){
						System.err.println("Fel vid hantering av anv�nder inmatning (MainWindow.java:#90)");
					}
					else if (userOption == JOptionPane.NO_OPTION){
						exitAndCloseProgram();
					}
				}
				else{
					exitAndCloseProgram();
				}

			}
		});

		/*	tblTimeEditor n;
		n = new tblTimeEditor(new tblTableModell(4,2012));

		n.setVisible(true);
		this.add(n);
		 */

	}

	public void setNeedSaving(boolean needSaving){
		this.needSaving = needSaving; 
		updateTitle();
	}
	public void updateTitle(){
		String title = system.WorkCalendar.APPLICATION_NAME +" "+system.WorkCalendar.SYSTEM_VERSION;
		if(Settings.instance().currentlyUsedFile() != null && Settings.instance().currentlyUsedFile().exists())
		{
			title = title+/*FILNAMN*/" - "+ Settings.instance().currentlyUsedFile().getName();
		}
		else{
			title = title+" - New Work Calendar.mwc";
		}
		if(needSaving){
			title = title+" *";
		}
		this.setTitle(title);
	}
	public void ExitProgram(){
		this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
	}
	private void exitAndCloseProgram(){
		if(!Settings.instance().saveSettings())
		{
			System.err.println("Errornous Program Termination, settings instance not saved correctly!");
		}
		dispose();
		System.err.close();
		System.exit(0);
	}
	public void updatePanelViews(){
		tabs.updatePanelViews();
	}
}
