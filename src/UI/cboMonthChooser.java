package UI;

import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;

import system.WorkDate;

public class cboMonthChooser<String> extends JComboBox<String> implements ItemListener {
	pnlView parent;
	public cboMonthChooser(pnlView parent){
		super();
		this.parent = parent;
		String monthInLetters;
		for(int n = 0; n< 12; n++){
			monthInLetters = (String) new WorkDate(2012,n+1,1).printMonth(WorkDate.LONG);
			this.addItem(monthInLetters);
		}
		this.addItemListener(this);
		WorkDate w = WorkDate.getSystemDate();
		this.setSelectedIndex(w.getMonth()-1);

		//S�tter preferredSize p� h�jden 
		this.setPreferredSize(new Dimension(this.getWidth(), 25));
	}
	@Override
	public void itemStateChanged(ItemEvent e) {
		parent.processedMonth = this.getSelectedIndex()+1;
		parent.updateTable();
		
	}
}
