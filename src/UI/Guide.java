package UI;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
public class Guide extends JFrame {
	public Guide() {
		super();
		setTitle("MNS WorkTime - Getting started");
		//setTitle("This program (1/5)");
		try {
			String html = Guide.getHTMLHeadAndCSS()+Guide.getHTMLPageOne();
			JEditorPane htmlContainer = new JEditorPane("text/html",html); 
			htmlContainer.setEditable(false);
			add(htmlContainer);
			setVisible(true);
			setSize(600,600);
			//setMinimumSize(new Dimension(1000,600));
			setBounds(350, 100, 650, 900);
			setResizable(false);
			//setDefaultCloseOperation();
		} catch(Exception e) {
			e.printStackTrace(); 
			System.out.println("Some problem has occured"+e.getMessage());
		} 
	} 

	private static String getHTMLHeadAndCSS(){
		String s = ""+
				"<html>"
				+ "<head><style>"
				+"h1{font-size:20px; text-align:center;}"
				+"h3{margin-bottom:0px;margin-top:5px;}"
				+"p{margin-top: 5px; font-size:12pt;}"
				+".more{font-size:10pt;color:#888888;}"
				+"body{background-color: #FEFEFE; font-family:verdana, sans-serif; padding: 20;}"
				+ "</style></head>";
		return s;
	}
	private static String getHTMLPageOne(){
		String s = ""+""
				+"<body height ='500'>"

				+ "<h1>Getting started with<br/>MahNasSoft WorkTime</h1>"
				+ "<h2>So what is this all about?</h2>"
				+ "<p style='font-size: 12px;'>This program is for registering time to track actuall worktime with the worktime you get paid for.</p><br/>"
				//H1
				+"<h3>Fill in time in the \"Edit\" tab</h3>"
				//H1 Main
				+"<p>Fill in the timestamp for check in/out of your card.<br/>"
				+ "For breaks, input '00:30' for half-hour breaks and so on...<br/>"
				+ "If you as an example want to input the time 17:38, just type '17:38' under relevant timestamp field.</p>"
				//H1 More
				+ "<p class=\"more\">Under every day you work you should fill in time for your work; "
				+ "begining time and end time, and don't forget about your break time!<br />There are different ways of inputing time. "
				+ "Of course, the easiest way is to do it properly, eg write 8 o clock as \"8:00\". Another way could be \"800\".<br />"
				+ "For breaks (if shorter than 1 hour) you could simply write \"30\" and it will automatically be interpreted as \"00:30\"<br/>"
				+ "If your break for a certain day is at least an hour, eg. 1h 15min, just input \"1:15\" or \"115\".</p>"
				//H2
				+"<h3>Check total hours/month in the \"Monthly Overview\" tab</h3>"
				//H2 Main
				+"<p>Here you will see details for calculated payed hours.<br/>"
				+ "At the bottom you will find the total hours you should get paid for in the end of the chosen month.<br/>"
				+ "Compare this total with your sallary note's total hours!</p>"
				//H2 More
				+"<p class=\"more\">In the end of every month, when your paycheck has arrived, control that the amount of hours you get paid for is the "
				+"same as what you actually worked. Note that a month has a different date-span than a normal calendar."
				+"E.g. You get sallary for June in the end of June, for worked hours between 16th of May to 15th of June.<br />"
				+"Also note: you only get sallary for whole quarters, i.e. work from 12:18 to 17:23 is counted as 12:15 to 17:30.</p>"
				//H3
				+"<h3>What are the \"Sick\" and the \"Personal Remarks\" columns used for?</h3>"
				//H3 Main
				+"<p>These is simply to make potentional errors traceable.<br/>"
				+ "Of course, you don't need to use these fields, or you could use them for other purposes you might find suite you.</p>"
				//H3 More
				+"<p class=\"more\">This is good because you can easily find if you get paid for days you reported sick (and you were supposed to get paid)."
				+"<br/>Personal remarks is to be able to find other error-factors, as if you forgot to scan your card or other stuff "
				+"that might help to track possible errors. Off course you can use it for other remarks too ;)</p>"
				//Disclaimer
				+"<h3>Disclaimer</h3>"
				+"<p>The developer cannot be held responisble for any kind of damage on the users computer, private economy or work.<br/>"
				+ "Use on your own responsibility!</p>"
				+"</body>"
				+"</html>";
		return s;
	}
	private static String getHTMLPageTwo(){
		String s = ""+""
				+"<body height ='500'>"
				+ "<h1>What is this all about?</h1>"
				+"</body>"
				+"</html>";
		return s;
	}
}