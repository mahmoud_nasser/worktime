package UI;

import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;
import javax.swing.SwingConstants;

import system.WorkDate;

public class cboYearChooser<String> extends JComboBox<String> implements ItemListener{
	pnlView parent;
	public cboYearChooser(pnlView parent){
		super();
		this.parent = parent;
		for(int n = 2005; n< 2035; n++){
			this.addItem((String) Integer.toString(n));
		}
		this.addItemListener(this);
		WorkDate w = WorkDate.getSystemDate();
		this.setSelectedItem(Integer.toString(w.getYear()));
		//S�tter preferredSize p� h�jden 
		this.setPreferredSize(new Dimension(this.getWidth(), 25));
	}
	@Override
	public void itemStateChanged(ItemEvent e) {
		parent.processedYear = Integer.parseInt(this.getSelectedItem().toString());
		parent.updateTable();
		
	}

}
