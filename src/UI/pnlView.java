package UI;

import javax.swing.JPanel;
import javax.swing.JTable;

public abstract class pnlView extends JPanel {
	protected int processedMonth, processedYear;
	protected JTable t;
	
	public abstract void updateTable();
}
