package UI.SummaryPanel;

import javax.swing.table.AbstractTableModel;

import UI.MainWindow;
import controllers.LanguageStringData;
import controllers.Settings;
import system.Time;
import system.WorkCalendar;
import system.WorkDate;

public class tblMonthSummarizerModell  extends AbstractTableModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String[] columnNames;
	private Object[][] data;
	private WorkCalendar wc;
	private String timeToAnotherSeparator;
	public int columnWidth[];

	tblMonthSummarizerModell(int paymentMonth, int paymentYear){
		this.wc = MainWindow.wc;
		timeToAnotherSeparator = " >>> ";
	//	Begin printing PaymentMonth (16/Month-1/Year to 15/Month/Year)
		WorkDate currentPrintDate = WorkDate.monthPaymentBegin(paymentYear, paymentMonth, Settings.instance().currentPayrollSystem); 
	//	int monthLength = WorkDate.getLengthOfMonth(paymentMonth-1, paymentYear);
		
	//	WorkDate currentPrintDate = new WorkDate(paymentYear, paymentMonth, 1); 
		WorkDate baseLengthFrom = currentPrintDate;
		int monthLength = WorkDate.getLengthOfMonth(baseLengthFrom.getMonth(), baseLengthFrom.getYear());
		
		WorkDate monthEnd = WorkDate.monthPaymentEnd(paymentYear, paymentMonth, Settings.instance().currentPayrollSystem);
		int columnCount = 6;
		columnNames = new String[columnCount];
		columnNames[0] = LanguageStringData.G_DATE();
		columnNames[1] = LanguageStringData.O_INCLUDED_TIMESTAMPS();
		columnNames[2] = LanguageStringData.G_BREAK();
		columnNames[3] = LanguageStringData.O_PAYED_TIME();
		columnNames[4] = LanguageStringData.G_SICK();
	//	columnNames[5] = "Inscannad";
		columnNames[5] = LanguageStringData.G_PERSONAL_REMARKS();
	


		data = new Object[monthLength][7]; //[Rader][Kolumner]
		//Calendar iterCal = new Calendar(wc.,m,y).getInstance
		for(int tableIndex=0; tableIndex<monthLength; tableIndex++){//For each date in between choosen month, eg Januari 2012, get 16 Dec 2011 to 15 Jan 2012
			data[tableIndex][0] = currentPrintDate.toString();
			//System.out.println("tableIndex: "+tableIndex+"\nDate: "+currentPrintDate+"\n\n");
			if(wc.get(currentPrintDate)==null){
				currentPrintDate = currentPrintDate.increaseDate();
				data[tableIndex][4] = false;
			//	data[tableIndex][5] = true;				
				continue;
			}
			Time timeBegunPayed = wc.get(currentPrintDate).timeBegunPayed();
			Time timeEndedPayed = wc.get(currentPrintDate).timeEndedPayed();
			Time payedPauseTime = wc.get(currentPrintDate).payedPauseTime();
			data[tableIndex][1] = timeBegunPayed.toString() + timeToAnotherSeparator + timeEndedPayed.toString();
			data[tableIndex][2] = wc.get(currentPrintDate).pauseTime().toString();
			data[tableIndex][3] = timeBegunPayed.timeDifference(timeEndedPayed).timeDifference(payedPauseTime).toString();
			data[tableIndex][4] = wc.get(currentPrintDate).getIllness();
		//	data[tableIndex][5] = wc.get(currentPrintDate).getCheckInCardIsScanned();
			data[tableIndex][5] = wc.get(currentPrintDate).getComment();
			if(currentPrintDate.hashCode() == monthEnd.hashCode()){
				break;
			}
			currentPrintDate = currentPrintDate.increaseDate();
			
		}

	}
	/*
	tblEditTableModell(){
		columnNames = new String[2];
		columnNames[0] = "B�rjade";
		columnNames[1] = "Slutade";


		data = new Object[4][2]; //[Rader][Kolumner]
		data[0][0] = "Hej";
		data[0][1] = new Boolean(true);
		data[1][0] = "Haj";
		data[1][1] = new Boolean(false);
		data[2][0] = null;
		data[2][1] = null;
		data[3][0] = "Seh�r";
		data[3][1] = new Boolean(true);
	}
	*/
	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return data.length;
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Object getValueAt(int row, int col) {
		return data[row][col];
	}

	public Class getColumnClass(int c) {
		//workaround
		Object value=this.getValueAt(0,c);  
		return (value==null?Object.class:value.getClass()); 
		//*
		//return getValueAt(0, c).getClass(); //original
	}

	/*
	 * Don't need to implement this method unless your table's
	 * editable.
	 */
	public boolean isCellEditable(int row, int col) {
		return false;
	}

	/*
	 * Don't need to implement this method unless your table's
	 * data can change.
	 */
	public void setValueAt(Object value, int row, int col) {
		data[row][col] = value;		
		fireTableCellUpdated(row, col);

	}
	
	
	
	}