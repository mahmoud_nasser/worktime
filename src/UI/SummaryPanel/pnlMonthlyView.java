package UI.SummaryPanel;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import UI.MainWindow;
import UI.cboMonthChooser;
import UI.cboYearChooser;
import UI.pnlView;

public class pnlMonthlyView extends pnlView implements TableModelListener{
	//	protected int processedMonth, processedYear;
	//	private JTable t;
	private lblMonthTotalPayed payedHours;
	public static pnlMonthlyView currInstance;
	public pnlMonthlyView(){
		currInstance = this;
		GroupLayout myLayout = new GroupLayout(this);
		setLayout(myLayout);
		//	GroupLayout.SequentialGroup leftToRight = myLayout.createSequentialGroup();
		//	GroupLayout.SequentialGroup topToBottom = myLayout.createSequentialGroup();



		t = new tblMonthSummarizer();
		//updateTable();
		JScrollPane scroll = new JScrollPane(t);
		payedHours = new lblMonthTotalPayed();

		JComboBox<String> cboMonthChooser = new cboMonthChooser<String>(this);
		JComboBox<String> cboYearChooser = new cboYearChooser<String>(this);
		//		.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
		//		.addGroup(myLayout.createSequentialGroup()

		myLayout.setHorizontalGroup(myLayout.createSequentialGroup()
				.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
						/*.addGroup(myLayout.createSequentialGroup()
								)*/
								.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addGroup(myLayout.createSequentialGroup()
												.addComponent(cboMonthChooser)
												.addComponent(cboYearChooser)
												)
										)
										.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
												.addComponent(scroll)
												)

												.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
														.addGroup(myLayout.createSequentialGroup()
																.addComponent(payedHours)
																)
														)
						)
				);

		myLayout.setVerticalGroup(myLayout.createSequentialGroup()
				/*.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
						)*/
						.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(cboMonthChooser)
								.addComponent(cboYearChooser)
								)

								.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addComponent(scroll)
										)	

										.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
												.addComponent(payedHours)
												)

				);
				//F�rhindrar Resize p� h�jden 
				myLayout.linkSize(SwingConstants.VERTICAL, cboMonthChooser, cboYearChooser);

		//updateTable();
		/*

		 GroupLayout.SequentialGroup buttons = myLayout.createSequentialGroup();

		 buttons.addComponent(btn1);
		 buttons.addComponent(btn2);
		 leftToRight.addGroup(buttons);
		 leftToRight.addComponent(t);
		 GroupLayout.ParallelGroup colOne = myLayout.createParallelGroup();
		 colOne.addComponent(btn1);
		 colOne.addComponent(btn2);
		// topToBottom.addGroup(myLayout.createParallelGroup(GroupLayout.Alignment.BASELINE));
		 topToBottom.addGroup(colOne);
		 topToBottom.addComponent(t);

		/*
		 http://www.java2s.com/Tutorial/Java/0240__Swing/AFramewithaGroupLayout.htm
		 */

		// myLayout.setHorizontalGroup(leftToRight);
		// myLayout.setVerticalGroup(topToBottom);
		myLayout.setAutoCreateGaps(true);
		myLayout.setAutoCreateContainerGaps(true);
	}


	@Override
	public void tableChanged(TableModelEvent e) {
		try{
			System.out.println("Countdown started!");
			Thread.sleep(100);
			for(int x=10;x>=0;x--)
			{
				System.out.println(x);
				Toolkit.getDefaultToolkit().beep();
				Thread.sleep(100);
			}            
		}
		catch(InterruptedException e2) { 

		}
		updateTable();
	}

	public void updateTable(){
		if(MainWindow.isTheProgramStartingUp){
			return;
		}
		t.setModel(new tblMonthSummarizerModell(processedMonth,processedYear));
		t.getTableHeader().setResizingAllowed(false);
		t.getTableHeader().setReorderingAllowed(false);
		t.getModel().addTableModelListener(this);
		payedHours.updateLabel(processedMonth,processedYear);
		//t.setCellSelectionEnabled(true);

	}
}
