package UI.SummaryPanel;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.text.JTextComponent;

public class tblMonthSummarizer extends JTable{
	private int columnWidth[];
	tblMonthSummarizer(){
		super();
		constructor();
	}
	tblMonthSummarizer(AbstractTableModel m){
		super(m);
		constructor();
	}
	
	@Override
	public void setModel(TableModel dataModel){
		super.setModel(dataModel);
		constructor();
	}

	@Override
	public boolean editCellAt(int row, int column, EventObject e)
	{
		boolean result = super.editCellAt(row, column, e);
		//constructor();
		selectAll(e);
		
		return result;
	}
	
	private void selectAll(EventObject e)
	{
		final Component editor = getEditorComponent();

		if (editor == null
		|| ! (editor instanceof JTextComponent))
			return;

		if (e == null)
		{
			((JTextComponent)editor).selectAll();
			return;
		}

		//  Typing in the cell was used to activate the editor

		if (e instanceof KeyEvent)
		{
			((JTextComponent)editor).selectAll();
			return;
		}

		//  F2 was used to activate the editor

		if (e instanceof ActionEvent)
		{
			((JTextComponent)editor).selectAll();
			return;
		}

		//  A mouse click was used to activate the editor.
		//  Generally this is a double click and the second mouse click is
		//  passed to the editor which would remove the text selection unless
		//  we use the invokeLater()

		if (e instanceof MouseEvent)
		{
			SwingUtilities.invokeLater(new Runnable()
			{
				public void run()
				{
					((JTextComponent)editor).selectAll();
				}
			});
		}
	}


	private void constructor(){
		//this.setSize(200,300);
		TableColumnModel cModel = this.getColumnModel();
		this.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,0), "selectNextColumnCell");
		this.setAutoResizeMode(AUTO_RESIZE_OFF);
		this.setColWidth();
		for(int n=0; n<cModel.getColumnCount();n++){
			cModel.getColumn(n).setPreferredWidth(columnWidth[n]);
		}
		this.setCellSelectionEnabled(false);
	}
	
	private void setColWidth(){
		columnWidth = new int[6];
		columnWidth[0] = 100;
		columnWidth[1] = 140;
		columnWidth[2] = 40;
		columnWidth[3] = 70;
		columnWidth[4] = 40;
	//	columnWidth[5] = 90;
		columnWidth[5] = 340;
	}
	
	
}
