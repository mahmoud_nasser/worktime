package UI.SummaryPanel;

import javax.swing.JLabel;

import UI.MainWindow;
import controllers.LanguageStringData;
import controllers.Settings;
import system.Day;
import system.Time;
import system.WorkDate;
import exception.TimeStructureException;


public class lblMonthTotalPayed extends JLabel {
	private static String HTML_INTRO = "<html>";
	private static String HTML_STYLE_1 = "<span style=\" font-family: Calibri, ; Font-size:10px; \" >";

	lblMonthTotalPayed(){
		super();
		this.setVisible(true);
	}
	private String calculateHours(int paymentYear, int paymentMonth){

		WorkDate currentProcessedDate = WorkDate.monthPaymentBegin(paymentYear, paymentMonth, Settings.instance().currentPayrollSystem); //Begin printing PaymentMont (16/Month-1/Year to 15/Month/Year
		int monthLength = WorkDate.getLengthOfMonth(paymentMonth-1, paymentYear);
		int h = 0;
		int m = 0;
		Time t;
		for(int tableIndex=0; tableIndex<monthLength; tableIndex++){//For each date in between choosen month, eg Januari 2012, get 16 Dec 2011 to 15 Jan 2012
			Day day = MainWindow.wc.get(new WorkDate(currentProcessedDate.getYear(), currentProcessedDate.getMonth(), currentProcessedDate.getDay()));
			currentProcessedDate = currentProcessedDate.increaseDate();
			if(day==null)
			{
				continue;
			}
			try {
				t = day.timePayed();
				h+=t.getHour();
				m+=t.getMinute();
			} catch (TimeStructureException e) {
				e.printStackTrace();
			}
		}
		h+= (int)m/60;
		m = m%60;
				
		return (h<10?"0"+h:h)+":"+(m<10?"0"+m:m);
	}
	public void updateLabel(int paymentMonth, int paymentYear){
		//String txt = HTML_INTRO+HTML_STYLE_1+"F�r denna m�nad borde du f� betalt f�r f�ljande tid: <strong>"+calculateHours(paymentYear, paymentMonth)+"</strong></span></html>";
		String[] strTid = calculateHours(paymentYear, paymentMonth).split(":");
		int[] t = {Integer.parseInt(strTid[0]),Integer.parseInt(strTid[1])};
		//String txt = "F�r denna m�nad borde du f� l�n f�r " + t[0] +(t[0]!=1?" timmar ":" timme ")+"och "+ t[1] + (t[1]!=1?" minuter.":" minut.");
		this.setText(LanguageStringData.O_SUMMARY_TEXT(t[0], t[1]));
	}

}
