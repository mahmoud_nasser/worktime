package exception;

public class XMLFileException extends Exception{
	private static final long serialVersionUID = 1L;
	String parsedTag;
	boolean tagNameInitiated;
	public XMLFileException(){
		this.tagNameInitiated = false;
	}
	public XMLFileException(String parsedTag){
		this.parsedTag = parsedTag;
		this.tagNameInitiated = true;
	}
	public void IPEmessage(){
		System.err.println(this.getStringMessage());
	}
	public void IPEmessage(String s){
		System.err.println(this.getStringMessage()+s);
	}
	
	private String getStringMessage(){
		if(tagNameInitiated){
			return "ConfigFileException: Value Parsing Error while reading tag " + parsedTag;
		}
		return "ConfigFileException: Value Parsing Error";
	}

}
