package system;

import java.io.Serializable;

import exception.TimeStructureException;

public class Time implements Serializable {
	private int h, min;
	public final static Time MIDNIGHT = new Time();
	
	private Time(){
		this.h = 0;
		this.min = 0;
	}
	public Time(int h, int min) throws TimeStructureException{
		if(h<0 || h>23|| min<0 || min>59)
			throw new TimeStructureException();
		
		this.h = h;
		this.min = min;
	}
	/**
	 * Does not support calculation over two days, will always make the [Most recent] - [Less recent]
	 * @return Returns the time difference
	 */
	public Time timeDifference(Time other){
		int startH;
		int startM;
		int endH;
		int endM;
		if((this.h<other.h)||((this.h==other.h)&&(this.min<other.min))){

			startH = this.h;
			startM = this.min;
			endH = other.h;
			endM = other.min;
		}
		else{
			startH = other.h;
			startM = other.min;
			endH = this.h;
			endM = this.min;
		}

		int diffH = 0;
		int diffM = 0;

		if(endM < startM){
			diffH = endH-startH-1;
			diffM = 60+endM-startM;
		}
		else{
			diffH = endH-startH;
			diffM = endM-startM;
		}


		Time result;
		try{
			result = new Time(diffH, diffM);
			return result;
		}
		catch(TimeStructureException e){
			System.err.println("Unexpected error, application terrminated");
			
		}
		System.exit(0);
		return null;
	}
	
	
	
	public int getHour(){
		return h;
	}
	public int getMinute(){
		return min;
	}
	public void setHour(int newHour) throws TimeStructureException {
		if(newHour<0 || newHour>23)
			throw new TimeStructureException();
		
		h = newHour;
		
	}
	public void setMinute(int newMinute) throws TimeStructureException {
		if(newMinute<0 || newMinute>59)
			throw new TimeStructureException();
		
		min = newMinute;
		
	}
	public void xklusiveSetMinute(int newMinute) throws TimeStructureException {
		if(newMinute<0 || newMinute>60)
			throw new TimeStructureException();
		
		min = newMinute;
		
	}
	public String toString(){
		String hh,mm;
		if(h<10){
			hh = "0"+h;
		}
		else{
			hh = String.valueOf(h);
		}
		if(min<10){
			mm = "0"+min;
		}
		else{
			mm = String.valueOf(min);
		}
		return hh+":"+mm;
	}
}
