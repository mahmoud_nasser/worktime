package system;
import java.io.Serializable;
public class Month implements Serializable{
	private static final long serialVersionUID = 1L;
	private WorkDate month;
	public Month(int y, int m){
		month = new WorkDate(y,m,1);
	}
	
	public String toString(){
		return month.getMonth()+"-"+month.getYear();
	}
	
	

}
