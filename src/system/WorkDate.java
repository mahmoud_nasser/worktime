package system;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import controllers.LanguageStringData;
import controllers.Settings;

public class WorkDate implements Serializable{
	
	
	private static final long serialVersionUID = 1L;
	//OLD Serialization Number... use to recover old files
	//private static final long serialVersionUID = 8438859931901204847L;
	private int y,m,d;
	public static boolean printDateWithMonthInThreeLetters = true;
	public final static int THREE_LETTER = GregorianCalendar.SHORT;
	public final static int LONG = GregorianCalendar.LONG;
	
	public WorkDate(int y, int m, int d){
		this.y = y;
		this.m = m;
		this.d = d;
	}
	/**
	 * Changes the current date for the Calendar to the given date
	 * @param y The year in 4 digits, eg "2012"
	 * @param menuFile The month, eg "4" or "11"
	 * @param d The day, eg "7" or "16"
	 **/

	public int getDay(){
		return new Integer(d);
	}
	public int getMonth(){
		return new Integer(m);
	}

	/**
	 * Returns the year of the calendars current date
	 * @return The year in 4 digits
	 */
	public int getYear(){
		return new Integer(y);
	}
	/**
	 * 
	 * @return Returns "1" for Monday... "7" for Sunday
	 * 
	 */
	public int getDayOfWeek(){
		GregorianCalendar def = new GregorianCalendar(y,m-1,d,0,0);	
		return new Integer(def.get(GregorianCalendar.DAY_OF_WEEK)-1);
	}
	
	public static int getLengthOfMonth(int month, int year){
		Calendar c = Calendar.getInstance();
		c.set(year, month-1, 1);
		return c.getActualMaximum(Calendar.DAY_OF_MONTH);
	}
	
	@Override
	public String toString(){
		String mm, dd, ww;
		if(WorkDate.printDateWithMonthInThreeLetters){
			mm = printMonth(THREE_LETTER);
		}
		else{
			if(m<10){
				mm = "0"+m;
			}
			else{
				mm = String.valueOf(m);
			}
		}
		if(d<10){
			dd = "0"+d;
		}
		else{
			dd = String.valueOf(d);
		}

		ww = printDayOfWeek(THREE_LETTER);
		return (/*ww+" "+*/dd+" "+mm+" "+y+", "+ ww/**/);
	}


	//Used for CompareTo(), do not modify!
	private int dateInInt(){
		String mm, dd;
		if(m<10){
			mm = "0"+m;
		}
		else{
			mm = String.valueOf(m);
		}
		if(d<10){
			dd = "0"+d;
		}
		else{
			dd = String.valueOf(d);
		}
		int date = Integer.parseInt(y+mm+dd);
		return date;
	}

	@Override
	public int hashCode(){
		return dateInInt();
	}

	@Override
	public boolean equals(Object that){
		if(that == null || !(that instanceof WorkDate)){
			return false;
		}
		else{
			return (this.dateInInt() == ((WorkDate) that).dateInInt());
		}
	}


	/**
	 * Compares the Date with another Date.
	 * 
	 * @param that The date you want to compare this day with
	 * @return returns 0 if both represent the same date, a value 
	 * less than zero if this Date is a later date than the argument 
	 * date and a value greater than zero if this date is an earlier S
	 * date than the argument date
	 */
	public int compareTo(WorkDate that){
		return that.dateInInt() - this.dateInInt();
	}


	public boolean isSunday(){
		GregorianCalendar g = new GregorianCalendar(y,m-1,d);
		return (g.get(GregorianCalendar.DAY_OF_WEEK)==GregorianCalendar.SUNDAY)?true:false;
	}

	public String printMonth(int lenthOfPrint){
		GregorianCalendar g = new GregorianCalendar(y,m-1,d);
		String returnVal = g.getDisplayName(g.MONTH, lenthOfPrint, LanguageStringData.getCurrentLocale());
		return Character.toUpperCase(returnVal.charAt(0)) + returnVal.substring(1);

	}
	public String printDayOfWeek(int lenthOfPrint){
		GregorianCalendar g = new GregorianCalendar(y,m-1,d);
		String returnVal = g.getDisplayName(g.DAY_OF_WEEK, g.LONG, LanguageStringData.getCurrentLocale());
		return Character.toUpperCase(returnVal.charAt(0)) +(lenthOfPrint==THREE_LETTER?returnVal.substring(1,3):returnVal);

	}
	public String print(){
		return d+"/"+m+"-"+y;
	}


	public WorkDate increaseDate(){
		Calendar c = Calendar.getInstance();
		c.set(y, m-1, d);
		int maxDay = c.getActualMaximum(Calendar.DAY_OF_MONTH);
		int tY, tM,tD;

		if(d==maxDay){
			if((m==12)){
				tD = 1;
				tM = 1;
				tY = y+1;
			}
			else{
				tD = 1;
				tM = m+1;
				tY = y;
			}

		}
		else{
			tD = d+1;
			tM = m;
			tY = y;
		}
		return new WorkDate(tY,tM,tD);
	}
	
	public WorkDate decreaseDate(){
		Calendar c = Calendar.getInstance();
		c.set(y, m-1, d);
		int tY, tM,tD;

		if(d==1){
			if((m==1)){
				tD = 31;
				tM = 12;
				tY = y-1;
			}
			else{
				tM = m-1;
				tY = y;
				Calendar ca = Calendar.getInstance();
				ca.set(tY, tM-1, 1);
				tD = ca.getActualMaximum(Calendar.DAY_OF_MONTH);;
			}

		}
		else{
			tD = d-1;
			tM = m;
			tY = y;
		}
		return new WorkDate(tY,tM,tD);
	}
	public static WorkDate monthPaymentBegin(int year, int month, int countrySystem){
		int tY, tM;
		/*if(month == 1){
			tM = 12;
			tY = year-1;
		}
		else{
			tM = month-1;
			tY = year;
		}
		*/

		WorkDate tW = (new WorkDate(year, month, 1)).decreaseDate();
		tY = tW.getYear();
		tM = tW.getMonth();
		switch(countrySystem){
		case Settings.PAYROLL_SYSTEM_DANISH:
			return new WorkDate(tY,tM,16);
		case Settings.PAYROLL_SYSTEM_SWEDISH: 
			return new WorkDate(tY, tM, 1);
		default :
			return new WorkDate(tY, tM, 1);
				
		}
	}
	public static WorkDate monthPaymentEnd(int year, int month, int countrySystem){
		switch(countrySystem){
		case Settings.PAYROLL_SYSTEM_DANISH:
			return new WorkDate(year,month,15);
		case Settings.PAYROLL_SYSTEM_SWEDISH: 
			return (new WorkDate(year, month, 1)).decreaseDate();
		default :
			return (new WorkDate(year, month, 1)).decreaseDate();
				
		}
	}
	/*NOT TESTED YET*/
	
	public static WorkDate getSystemDate(){
		GregorianCalendar g = new GregorianCalendar();
		int year = g.get(GregorianCalendar.YEAR);
		int month = g.get(GregorianCalendar.MONTH)+1;
		int day = g.get(GregorianCalendar.DAY_OF_MONTH);
		return new WorkDate(year, month, day);
		
	}

	
}
