/**
 * 
 */
package system;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

/**
 * @author Mahmoud
 *
 */
public class WorkCalendar implements Serializable{

	public static final String SYSTEM_VERSION = "1.3";
	public static final String APPLICATION_NAME = "WorkTime";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	HashMap<WorkDate, Day> database;

	/**
	 * Creates the end-user calendar based on a Hashmap with a "Workdate" as a key and a "Day" as value
	 */
	public WorkCalendar() {
		database = new HashMap<WorkDate, Day>();
	}
	
	public Day put(WorkDate key, Day value){
		if(key==null || value==null){
			return null;
		}
		return database.put(key, value);
	}
	public Day get(WorkDate key){
		Day tempDay = database.get(key);
		if(tempDay != null){
			return tempDay;
		}
		
		return null;
	}
	
	public boolean containsKey(WorkDate key){
		return database.containsKey(key);
	}
	/**
	 * Removes an Entry from the given WorkDate from the WorkCalendar
	 * @param key The WorkDay on this WorkDate that should be removed
	 * @return true whenever a value was found and removed, 
	 */
	public boolean removeEntry(WorkDate key){
		if(!database.containsKey(key)){
			return false;
		}
		Day value = database.remove(key);
		return (value != null);
		
		
	}
	public int size(){
		return database.size();
	}
	public Set<Entry<WorkDate, Day>> getHashMap(){
		return database.entrySet();
	}


}
