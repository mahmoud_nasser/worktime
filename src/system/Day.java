package system;

import java.io.Serializable;

import exception.TimeStructureException;

public class Day implements Serializable {
	//	public static Time NIGHT_TIME_BEGIN, NIGHT_TIME_END; //Implementeras
	private Time start, end;
	private Time payedStart, payedEnd;
	private Time pause, payedPause;
	//	private Time day, night;							//Implementeras
	//	private boolean isSaturday, isSunday;	
	private String comment;
	private boolean checkInCardIsScanned;
	private boolean isIll;
	//L�gg till/implementera
	//>L�s s� att Default-v�rde f�r paus och checkInCardIsScanned kan �ndras p� ett praktiskt s�tt
	//start & end & paus �r f�r GUI, payed* �r f�r att ber�kna betald tid (mer eller mindre internt)


	/*
	 * Implementeras (Saknar testfall)
	public void INIT_STATICS(){
		try{
			NIGHT_TIME_BEGIN = new Time(18,00);
			NIGHT_TIME_END = new Time(6,00);
		}
		catch(TimeFormatException e){
			System.err.println("Error on Static data initialisation!\nDay.java");
		}

	}
	 */
	public Day(){
			Constructor(Time.MIDNIGHT, Time.MIDNIGHT, Time.MIDNIGHT);
	}
	public Day(Time start, Time end, Time pause){
		Constructor(start, end, pause);
	}

	public Day(Time start, Time end){
		try{
			//H�rdkodad default paus - tid
			Constructor(start, end, new Time(0, 0));
		}
		catch(TimeStructureException e){
			e.IPEmessage("in Day.java:53)");
		}
	}

	private void Constructor(Time start, Time end, Time pause) {
		//	INIT_STATICS();	
		this.start = start;
		this.end = end;
		this.pause = pause;
		this.comment = "";
		this.checkInCardIsScanned = true;
		this.isIll = false;
		try{
			payedStart = new Time(0,0);
			payedEnd = new Time(0,0);
			payedPause = new Time(0,0);
		}
		catch(TimeStructureException e){
			e.IPEmessage("in Day.java:71)");
		}





	}
	/*
	public Time timeWorkedDay(){
		updateWorkHourType();
		return pause.timeDifference(start.timeDifference(end));
	}
	public Time timeWorkedNight(){
		updateWorkHourType();
		return pause.timeDifference(start.timeDifference(end));
	}
	 */
	public Time timeWorked(){
		return pause.timeDifference(start.timeDifference(end));
	}

	public Time timePayed() throws TimeStructureException{
		updatePayedTime();
		return payedPause.timeDifference(payedStart.timeDifference(payedEnd));
	}

	//	private void updateWorkHourType(){
	//	for sats som g�r igenom alla timmar och sorterar r�tt i vanliga timmar och i kv�llstimmar
	//	}

	private void updatePayedTime(){
		try{
			payedStart.setHour(start.getHour());
			int temp = minuteRegulator(start.getMinute());
			if(temp>= 60){
				payedStart.setMinute(0);
				payedStart.setHour(start.getHour()+1);
			}
			else{
				payedStart.setMinute(temp);
			}

			payedEnd.setHour(end.getHour());
			temp = minuteRegulator(end.getMinute());
			if(temp >= 60){
				payedEnd.setMinute(0);
				payedEnd.setHour(end.getHour()+1);
			}
			else{
				payedEnd.setMinute(temp);
			}

			payedPause.setHour(pause.getHour());
			temp = minuteRegulator(pause.getMinute());
			if(temp >= 60){
				payedPause.setMinute(0);
				payedPause.setHour(pause.getHour()+1);
			}
			else{
				payedPause.setMinute(temp);
			}

		}
		catch(TimeStructureException e){
			e.IPEmessage("in Day.java:136)");
		}
	}

	private int minuteRegulator(int realMinutes){
		int returnValue = 0;
		if (realMinutes == 0 || realMinutes == 15 || realMinutes == 30 || realMinutes == 45){
			returnValue = realMinutes;
		}
		else if (realMinutes >= 53){
			returnValue = 60;
		}
		else if (realMinutes >= 0 && realMinutes <= 7){
			returnValue = 0;
		}
		else if (realMinutes >= 8 && realMinutes <= 22){
			returnValue = 15;
		}
		else if (realMinutes >= 23 && realMinutes <= 37){
			returnValue = 30;
		}
		else if (realMinutes >= 38 && realMinutes <= 52){
			returnValue = 45;
		}
		return returnValue;
	}




	public String toString(){
		return start.toString()+" to "+end.toString();
	}

	public void setComment(String newComment){
		this.comment = newComment;
	}
	public String getComment(){
		return this.comment;
	}

	public void setCheckInCardIsScanned(boolean isScanned){
		this.checkInCardIsScanned = isScanned;
	}
	public boolean getCheckInCardIsScanned(){
		return this.checkInCardIsScanned;
	}

	public void setIllness(boolean isIll){
		this.isIll = isIll;
	}
	public boolean getIllness(){
		return this.isIll;
	}

	public Time pauseTime(){
		return pause;
	}
	public Time timeBegun() {
		return start;
	}

	public Time payedPauseTime(){
		updatePayedTime();
		return payedPause;
	}
	public Time timeEnded() {
		return end;
	}

	public Time timeBegunPayed() {
		updatePayedTime();
		return payedStart;
	}

	public Time timeEndedPayed() {
		updatePayedTime();

		return payedEnd;
	}

	//public void updatePayedHoursDayOrNight(){ //R�kna hur m�nga timmar man f�r som kv�llstid}
}
