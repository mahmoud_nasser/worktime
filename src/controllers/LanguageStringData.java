package controllers;

import java.util.Locale;

public class LanguageStringData {
	/** METHOD PREFIX LEGEND
	 * Common/Global (G)
	 * Edit tab (E)
	 * Month overview tab (O)
	 * Menus (M)
	 * JFrame (J)
	 * File handling (F)
	 */
	/* ** *** **** ***** ****** ******* ****** ***** **** *** ** */
	public static Locale getCurrentLocale(){
		Locale retVal = Locale.ENGLISH;
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = Locale.ENGLISH;
			break;
		case Settings.LANG_SWE:
			retVal = new Locale("sv", "SE");
			break;
		case Settings.LANG_DAN:
			retVal = new Locale("da", "DK");
			break;
		}
		return retVal;
	}

	/* ** *** **** ***** ****** ******* ****** ***** **** *** ** */

	/**
	 * Returns text for the "Date"
	 */
	public static String G_DATE(){
		String retVal = "Date";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Date";
			break;
		case Settings.LANG_SWE:
			retVal = "Datum";
			break;
		case Settings.LANG_DAN:
			retVal = "Dato";
			break;
		}
		return retVal;
	}
	/**
	 * Returns text for "Break" (as in "pause")
	 */
	public static String G_BREAK(){
		String retVal = "Break";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Break";
			break;
		case Settings.LANG_SWE:
			retVal = "Rast";
			break;
		case Settings.LANG_DAN:
			retVal = "Pause";
			break;
		}
		return retVal;
	}
	/**
	 * Returns text for "Sick"
	 */
	public static String G_SICK(){
		String retVal = "Sick";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Sick";
			break;
		case Settings.LANG_SWE:
			retVal = "Sjuk";
			break;
		case Settings.LANG_DAN:
			retVal = "Syg";
			break;
		}
		return retVal;
	}
	/**
	 * Returns text for "Personal Remarks"
	 */
	public static String G_PERSONAL_REMARKS(){
		String retVal = "Personal Remarks";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Personal Remarks";
			break;
		case Settings.LANG_SWE:
			retVal = "Personliga Anm�rkningar";
			break;
		case Settings.LANG_DAN:
			retVal = "Personlige Bem�rkninger";
			break;
		}
		return retVal;
	}

	/* ** *** **** ***** ****** ******* ****** ***** **** *** ** */

	/**
	 * Returns text for the Edit tab title ("Edit")
	 */
	public static String E_TITLE(){
		String retVal = "Edit";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Edit";
			break;
		case Settings.LANG_SWE:
			retVal = "Redigera";
			break;
		case Settings.LANG_DAN:
			retVal = "Rediger";
			break;
		}
		return retVal;
	}
	/**
	 * Returns text for the Edit tab tooltip
	 */
	public static String E_TOOLTIP(){
		String retVal = "Add and remove workdays";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Add and remove workdays";
			break;
		case Settings.LANG_SWE:
			retVal = "L�gg till och ta bort arbetsdagar";
			break;
		case Settings.LANG_DAN:
			retVal = "Tilf�je og slette dage";
			break;
		}
		return retVal;
	}
	/**
	 * Returns text for "Begun"
	 */
	public static String E_BEGUN(){
		String retVal = "Begun";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Begun";
			break;
		case Settings.LANG_SWE:
			retVal = "B�rjade";
			break;
		case Settings.LANG_DAN:
			retVal = "Begyndte";
			break;
		}
		return retVal;
	}
	/**
	 * Returns text for "Ended"
	 */
	public static String E_ENDED(){
		String retVal = "Ended";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Ended";
			break;
		case Settings.LANG_SWE:
			retVal = "Slutade";
			break;
		case Settings.LANG_DAN:
			retVal = "Sluttede";
			break;
		}
		return retVal;
	}

	/* ** *** **** ***** ****** ******* ****** ***** **** *** ** */

	/**
	 * Returns text for the Month Overview tab title ("Month Overview")
	 */
	public static  String O_TITLE(){
		String retVal = "Edit";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Monthly Overview";
			break;
		case Settings.LANG_SWE:
			retVal = "M�natlig �versikt";
			break;
		case Settings.LANG_DAN:
			retVal = "M�nedlig Oversigt";
			break;
		}
		return retVal;
	}
	/**
	 * Returns text for the Month Overview tab tooltip
	 */
	public static  String O_TOOLTIP(){
		String retVal = "Get an overview of given months";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Get an overview of given months";
			break;
		case Settings.LANG_SWE:
			retVal = "F� en �versikt �ver givna m�nader";
			break;
		case Settings.LANG_DAN:
			retVal = "F� et overblik over givne m�neder";
			break;
		}
		return retVal;
	}
	/**
	 * Returns text for "Included Timpestamps"
	 */
	public static String O_INCLUDED_TIMESTAMPS(){
		String retVal = "Included Timpestamps";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Included Timpestamps";
			break;
		case Settings.LANG_SWE:
			retVal = "Inr�knade Tidsst�mplar";
			break;
		case Settings.LANG_DAN:
			retVal = "Inkluderede Tidsstempler";
			break;
		}
		return retVal;
	}	
	/**
	 * Returns text for "Payed Time"
	 */
	public static String O_PAYED_TIME(){
		String retVal = "Paid Time";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Paid Time";
			break;
		case Settings.LANG_SWE:
			retVal = "Betald Tid";
			break;
		case Settings.LANG_DAN:
			retVal = "Betalt Tid";
			break;
		}
		return retVal;
	}
	/**
	 * Returns text for the text telling the user how many hours and minutes s-/he need to be paid for
	 */
	public static String O_SUMMARY_TEXT(int hours, int minutes){
		String retVal = "For this month you should get paid for "+
				hours +(hours!=1?" hours":" hour")+" and "+ minutes + (minutes!=1?" minutes.":" minute.");
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "For this month you should get paid for "+
					hours +(hours!=1?" hours":" hour")+" and "+ minutes + (minutes!=1?" minutes.":" minute.");
			break;
		case Settings.LANG_SWE:
			retVal = "F�r denna m�nad borde du f� l�n f�r "+ 
					hours +(hours!=1?" timmar":" timme")+" och "+ minutes + (minutes!=1?" minuter.":" minut.");
			break;
		case Settings.LANG_DAN:
			/* E.g. "For denne m�ned burde du f� l�n for 10 timer og 45 minuter" */
			retVal = "For denne m�ned burde du f� l�n for "+
					hours +(hours!=1?" timer":" time")+" og "+ minutes + (minutes!=1?" minutter.":" minut.");
			break;
		}
		return retVal;
	}

	/* ** *** **** ***** ****** ******* * ******* ****** ***** **** *** ** */
	
	/**
	 * Returns text for "File" menu
	 */
	public static String M_FILE(){
		String retVal = "File";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "File";
			break;
		case Settings.LANG_SWE:
			retVal = "Arkiv";
			break;
		case Settings.LANG_DAN:
			retVal = "Filer";
			break;
		}
		return retVal;
	}
	/**
	 * Returns text for File > "New Calendar" menu
	 */
	public static String M_E_NEW(){
		String retVal = "New Calendar";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "New Calendar";
			break;
		case Settings.LANG_SWE:
			retVal = "Ny Kalender";
			break;
		case Settings.LANG_DAN:
			retVal = "Ny Kalender";
			break;
		}
		return retVal;
	}
	/**
	 * Returns text for File > "Open..." menu
	 */
	public static String M_E_OPEN(){
		String retVal = "Open...";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Open...";
			break;
		case Settings.LANG_SWE:
			retVal = "�ppna...";
			break;
		case Settings.LANG_DAN:
			retVal = "�bn...";
			break;
		}
		return retVal;
	}
	/**
	 * Returns text for File > "Save" menu
	 */
	public static String M_E_SAVE(){
		String retVal = "Save";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Save";
			break;
		case Settings.LANG_SWE:
			retVal = "Spara";
			break;
		case Settings.LANG_DAN:
			retVal = "Gem";
			break;
		}
		return retVal;
	}
	/**
	 * Returns text for File > "Save as..." menu
	 */
	public static String M_E_SAVE_AS(){
		String retVal = "Save as...";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Save as...";
			break;
		case Settings.LANG_SWE:
			retVal = "Spara som...";
			break;
		case Settings.LANG_DAN:
			retVal = "Gem som...";
			break;
		}
		return retVal;
	}
	/**
	 * Returns text for File > "Quit" menu
	 */
	public static String M_E_QUIT(){
		String retVal = "Quit";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Quit";
			break;
		case Settings.LANG_SWE:
			retVal = "Avsluta";
			break;
		case Settings.LANG_DAN:
			retVal = "Afslut";
			break;
		}
		return retVal;
	}

	/**
	 * Returns text for "Edit" menu
	 */
	public static String M_EDIT(){
		String retVal = "Edit";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Edit";
			break;
		case Settings.LANG_SWE:
			retVal = "Redigera";
			break;
		case Settings.LANG_DAN:
			retVal = "Rediger";
			break;
		}
		return retVal;
	}
	
	
	/**
	 * Returns text for "Language" menu
	 */
	public static String M_E_LANGUAGE(){
		String retVal = "Language";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Language";
			break;
		case Settings.LANG_SWE:
			retVal = "Spr�k";
			break;
		case Settings.LANG_DAN:
			retVal = "Sprog";
			break;
		}
		return retVal;
	}
	/**
	 * Language text used for Language items in "Language" menu
	 * 
	 * Note - text only, no translation
	 */
	public static String M_E_L_ITM(int langID){
		String retVal = "<Undefined>";
		switch(langID){
		case Settings.LANG_ENG:
			retVal = "English";
			break;
		case Settings.LANG_SWE:
			retVal = "Svenska";
			break;
		case Settings.LANG_DAN:
			retVal = "Dansk";
			break;
		}
		return retVal;
	}
	/**
	 * Returns text for "Payroll System" menu
	 */
	public static String M_E_PAYROLL_SYSTEM(){
		String retVal = "Payroll System";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Payroll System";
			break;
		case Settings.LANG_SWE:
			retVal = "L�nesystem";
			break;
		case Settings.LANG_DAN:
			retVal = "L�nsystem";
			break;
		}
		return retVal;
	}
	/**
	 * Returns the text "Swedish" used in e.g the menu Payroll System > "Swedish"
	 */
	public static String M_E_PS_SWEDISH(){
		String retVal = "Swedish";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Swedish";
			break;
		case Settings.LANG_SWE:
			retVal = "Svenskt";
			break;
		case Settings.LANG_DAN:
			retVal = "Svensk";
			break;
		}
		return retVal;
	}
	/**
	 * Returns the text "Danish" used in e.g the menu Payroll System > "Danish"
	 */
	public static String M_E_PS_DANISH(){
		String retVal = "Danish";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Danish";
			break;
		case Settings.LANG_SWE:
			retVal = "Danskt";
			break;
		case Settings.LANG_DAN:
			retVal = "Dansk";
			break;
		}
		return retVal;
	}
	/**
	 * Returns text for "Help" menu
	 */
	public static String M_HELP(){
		String retVal = "Help";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Help";
			break;
		case Settings.LANG_SWE:
			retVal = "Hj�lp";
			break;
		case Settings.LANG_DAN:
			retVal = "Hj�lp";
			break;
		}
		return retVal;
	}
	/**
	 * Returns the text "Getting Started" used in e.g the menu Help > "Getting Started"
	 */
	public static String M_H_GETTING_STARTED(){
		String retVal = "Getting Started...";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Getting Started...";
			break;
		case Settings.LANG_SWE:
			retVal = "Kom Ig�ng...";
			break;
		case Settings.LANG_DAN:
			retVal = "Kom i Gang...";
			break;
		}
		return retVal;
	}
	/**
	 * Returns the text "About" used in e.g the menu Help > "About WorkTime"
	 */
	public static String M_H_ABOUT(){
		String retVal = "About";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "About";
			break;
		case Settings.LANG_SWE:
			retVal = "Om";
			break;
		case Settings.LANG_DAN:
			retVal = "Om";
			break;
		}
		return retVal;
	}
	public static String M_H_ISSUES() {
		String retVal = "Known Issues";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Known Issues";
			break;
		case Settings.LANG_SWE:
			retVal = "K�nda Problem";
			break;
		case Settings.LANG_DAN:
			retVal = "Kendte Problemer";
			break;
		}
		return retVal;
	}
	
	/* ** *** **** ***** ****** ******* * ******* ****** ***** **** *** ** */
	
	/**
	 * Text used in JDialog  when informing user to restart for new (Language)-settings
	 */
	public static String J_CONFIRM_RESTART_SETTINGS(){
		return J_CONFIRM_RESTART_SETTINGS(Settings.instance().getLanguage());
	}
	/**
	 * Text used in JDialog when informing user to restart for new (Language)-settings in given language
	 */
	public static String J_CONFIRM_RESTART_SETTINGS(int LangID){
		String retVal = "The program will need to restart in order to apply the new settings."+
				"\nWarning: Unsaved changes will NOT will be lost!\nQuit anyway?";
		switch(LangID){
		case Settings.LANG_ENG:
			retVal = "The program will need to restart in order to apply the new settings."+
					"\nWarning: Unsaved changes will be lost\nQuit anyway?";
			break;
		case Settings.LANG_SWE:
			retVal = "Programmet beh�ver startas om f�r att verkst�lla de nya inst�llningarna."+
					"\nVarning: Osparade f�r�ndringar g�r f�rlorade!\nAvsluta �nd�?";
			break;
		case Settings.LANG_DAN:
			retVal = "Programmet kr�ver genstart for at anvende de nye indstillinger."+
					"\nAdvarsel: Ikke gemte �ndringer bliver mistet!\nAfslut alligevel?";
			break;
		}
		return retVal;
	}
	/**
	 * Text used in JDialog when informing user to restart for new (Language)-settings in given language
	 */
	public static String J_WELCOM_NEW_USER(){
		String retVal = "Welcome!\nIf this is the first time you use this program and you feel confuesed, feel free to klick ";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Welcome!\nIf this is the first time you use this program and you feel confuesed, feel free to klick ";
			break;
		case Settings.LANG_SWE:
			retVal = "V�lkommen!\nOm det h�r �r f�rsta g�ngen du anv�nder programmet och du k�nner dig vilsen, titta g�rna i ";
			break;
		case Settings.LANG_DAN:
			retVal = "Velkomen!\nHvis det her er f�rste gang du bruger programmet og finder dig selv forvirret, kige gerne i ";
			break;
		}
		retVal+=LanguageStringData.M_HELP()+" > "+ LanguageStringData.M_H_GETTING_STARTED()+"!";
		return retVal;
	}
	
	/* ** *** **** ***** ****** ******* * ******* ****** ***** **** *** ** */
	
	public static String F_QUIT_UNSAVED_CHANGES_TITLE(){
		String retVal = "Unsaved Changes Detected";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Detected Unsaved Changes";
			break;
		case Settings.LANG_SWE:
			retVal = "Osparade �ndringar Uppt�ckta";
			break;
		case Settings.LANG_DAN:
			retVal = "Ugemte �ndringer Fundne";
			break;
		}
		return retVal;
	}
	public static String F_QUIT_UNSAVED_CHANGES(){
		String retVal = "Unsaved changes detected!\nDo you want to save changes in the calendar before you quit?";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Unsaved changes detected!\nDo you want to save changes in the calendar before you quit?";
			break;
		case Settings.LANG_SWE:
			retVal = "Osparade �ndringar uppt�ckta!\nVill du spara �ndringar i kalendern innan du avslutar?";
			break;
		case Settings.LANG_DAN:
			retVal = "Ugemte �ndringer fundne!\nVil du gemme �ndringer i kalendern f�r du afsluster?";
			break;
		}
		return retVal;
	}
	

	public static String F_UNSAVED_CHANGES_TITLE(){
		String retVal = "Unsaved Changes Detected";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Detected Unsaved Changes";
			break;
		case Settings.LANG_SWE:
			retVal = "Osparade �ndringar Uppt�ckta";
			break;
		case Settings.LANG_DAN:
			retVal = "Ugemte �ndringer Fundne";
			break;
		}
		return retVal;
	}
	public static String F_UNSAVED_CHANGES(){
		String retVal = "Unsaved changes detected!\nUnsaved changes will be lost, continue?";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Unsaved changes detected!\nUnsaved changes will be lost, continue?";
			break;
		case Settings.LANG_SWE:
			retVal = "Osparade �ndringar uppt�ckta!\nOsparade �ndringar g�r f�rlorade, vill du forts�tta?";
			break;
		case Settings.LANG_DAN:
			retVal = "Ugemte �ndringer fundne!\nUgemte �ndringer bliver mistet, vil du forts�tte?";
			break;
		}
		return retVal;
	}
	public static String F_OPEN_UNSUPPORTED_FILE_TITLE(){
		String retVal = "Unsupported or Corrupt File";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Unsupported or Corrupt File";
			break;
		case Settings.LANG_SWE:
			retVal = "Ogiltig eller Korrupt Fil";
			break;
		case Settings.LANG_DAN:
			retVal = "Ugyldig eller Korrupt Fil";
			break;
		}
		return retVal;
	}
	public static String F_OPEN_UNSUPPORTED_FILE(){
		String retVal = "The file you are trying to open is not supported by the program, or the file is corrupt!";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "The file you are trying to open is not supported by the program, or the file is corrupt!";
			break;
		case Settings.LANG_SWE:
			retVal = "Filen du f�rs�ker �ppna st�ds inte av programmet, eller s� �r filen korrupt!";
			break;
		case Settings.LANG_DAN:
			retVal = "Den fil, du fors�ger at �bne, er ikke underst�ttet af programmet, eller s� er filen beskadiget!";
			break;
		}
		return retVal;
	}
	
	public static String F_SAVE_OVERWRITE_TITLE(){
		String retVal = "File Already Exists";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "File Already Exists";
			break;
		case Settings.LANG_SWE:
			retVal = "Fil Existerar Redan";
			break;
		case Settings.LANG_DAN:
			retVal = "Fil Eksisterer Allerede";
			break;
		}
		return retVal;
	}
	public static String F_SAVE_OVERWRITE(){
		String retVal = "Inputed filename already exists.\nDo you wish to overwrite the file?";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Inputed filename already exists.\nDo you wish to overwrite the file?";
			break;
		case Settings.LANG_SWE:
			retVal = "Angiven filnamn existerar redan.\nVill du skriva �ver filen?";
			break;
		case Settings.LANG_DAN:
			retVal = "Den angivne filnavn eksisterer allerede.\nVil du overskrive filen?";
			break;
		}
		return retVal;
	}
	public static String F_SAVE_ERROR_TITLE(){
		String retVal = "Unknown Error When Saving To File";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "Unknown Error When Saving To File";
			break;
		case Settings.LANG_SWE:
			retVal = "Ok�nt Fel Vid Sparandet Till Fil";
			break;
		case Settings.LANG_DAN:
			retVal = "Okendt Fejl Vid Lagring Til Fil ";
			break;
		}
		return retVal;
	}
	public static String  F_SAVE_ERROR(){
		String retVal = "An unknown error was encounterd when saving to file!";
		switch(Settings.instance().getLanguage()){
		case Settings.LANG_ENG:
			retVal = "An unknown error was encountered when saving to file!";
			break;
		case Settings.LANG_SWE:
			retVal = "Ett ok�nt fel st�ttes p� vid sparandet till fil!";
			break;
		case Settings.LANG_DAN:
			retVal = "Der opstod en ukendt fejl under fors�get p� at gemme filen!";
			break;
		}
		return retVal;
	}



	
}
