package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.swing.JOptionPane;

import system.WorkCalendar;
import UI.MainWindow;
import filehandling.FileHandler;

public class Settings implements Serializable {
	private static Settings instance = null;
	public boolean firstTimeUser;
	private int currentLanguage;
	public int currentPayrollSystem;

	private static final long serialVersionUID = 1L;
	private static final String SETTINGS_FILE_NAME = "WorkTime.cfg";
	
	public static final int LANG_ENG = 0;
	public static final int LANG_SWE = 1;
	public static final int LANG_DAN = 2;
	

	public static final int PAYROLL_SYSTEM_SWEDISH = 0;
	public static final int PAYROLL_SYSTEM_DANISH = 1;

	//Settings to save/load (Default)
	private File currentlyUsedFile;
	private String lastUsedFileChooserDir;
	//End of Settings

	//public static String LAST_USED_FILECHOOSER_DIR = System.getProperty("user.home")+File.separator+"Desktop";


	private Settings(){
		currentlyUsedFile = null;
		lastUsedFileChooserDir = System.getProperty("user.home")+File.separator+"Desktop";
	}


	public static Settings instance(){
		if(instance == null){
			instance = new Settings();
			instance.loadSettings();
		}
		return instance;
	}


	public File currentlyUsedFile(){
		return currentlyUsedFile;
	}
	public void currentlyUsedFile(File currentlyUsedFile){
		this.currentlyUsedFile = currentlyUsedFile;
	}

	public String lastUsedFileChooserDir(){
		return lastUsedFileChooserDir;
	}
	public void lastUsedFileChooserDir(String lastUsedFileChooserDir){
		this.lastUsedFileChooserDir = lastUsedFileChooserDir;
	}



	/**
	 * Loads and applies settings from a hard-coded config file.
	 * Creates a file with default settings if such file does not exist
	 * @return Returns true on if load or default settings loads correctly
	 */
	public boolean loadSettings(){
		try{
			Object obj;
			//Ladda in r�tt WC
			if(!(currentlyUsedFile==null) && currentlyUsedFile.exists()){
				obj = FileHandler.readObjectFromFile(currentlyUsedFile);
				if (obj instanceof WorkCalendar)
				{
					MainWindow.wc = (WorkCalendar) obj;
				}
			}else{
				MainWindow.wc = new WorkCalendar();
			}
			obj = FileHandler.readObjectFromFile(new File(SETTINGS_FILE_NAME));
			if (obj instanceof Settings)
			{
				Settings.instance = (Settings) obj;
			}else{
				throw new FileNotFoundException();
			}
			firstTimeUser = false;
			return true;
		}
		catch(FileNotFoundException e){
			firstTimeUser = true;
			currentLanguage = LANG_ENG;
			currentPayrollSystem = PAYROLL_SYSTEM_SWEDISH;
			System.out.println("FNFE");
			return saveDefaultSettings();
		}
		catch(IOException e) {
			System.out.println("IOE");
			return false;
		}
		catch (ClassNotFoundException e) {
			System.out.println("CNFE");
			return false;
		}		
	}

	/**
	 * Saves currently applied settings to a hard-coded config file.
	 * @return Returns true on success and false when failing to save
	 */
	public boolean saveSettings(){
		// Write to disk with FileOutputStream
		FileOutputStream f_out;
		try {
			f_out = new FileOutputStream(SETTINGS_FILE_NAME);
			ObjectOutputStream obj_out = new ObjectOutputStream (f_out);
			obj_out.writeObject(Settings.instance);
			obj_out.close();
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * This method is most likely called when the user starts the program for the first time.
	 * The method saves the applications default settings to a file, thus creating a file.
	 * @return Returns true on success and false when failing to save
	 */
	private boolean saveDefaultSettings(){
		if(firstTimeUser){
			JOptionPane.showMessageDialog(MainWindow.MAIN_WINDOW, LanguageStringData.J_WELCOM_NEW_USER(), "Welcome", JOptionPane.INFORMATION_MESSAGE);
			firstTimeUser = false;
		}
		return saveSettings();
	}
	public int getLanguage() {
		return currentLanguage;

	}

	public void setLanguage(int langID) {
		if(langID == currentLanguage){
			return;
		}
		int choice = JOptionPane.showConfirmDialog(MainWindow.MAIN_WINDOW, LanguageStringData.J_CONFIRM_RESTART_SETTINGS()+
				"\n\n"+LanguageStringData.J_CONFIRM_RESTART_SETTINGS(langID),LanguageStringData.M_E_LANGUAGE(), JOptionPane.YES_NO_OPTION);
		if(choice == JOptionPane.CANCEL_OPTION || choice == JOptionPane.NO_OPTION){
			return;
		}
		else if(choice == JOptionPane.YES_OPTION){
			currentLanguage = langID;
			saveSettings();
			MainWindow.MAIN_WINDOW.dispose();
			MainWindow.MAIN_WINDOW = new MainWindow();
			return;
		}

	}

}
