package system;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import exception.TimeStructureException;

import system.Day;
import system.Time;

public class DayTest {
	Day d1, d2;
	@Before
	public void setUp() throws TimeStructureException{
		d1 = new Day(new Time(7,9), new Time(16,48));
		d2 = new Day(new Time(7,9), new Time(16,48), new Time(0,25));
	}
	@Test
	public void emptyConstructor() throws TimeStructureException {
		Day d3 = new Day();
		assertTrue(d3.timeBegun().getHour()==0 && d3.timeBegun().getMinute()==0); 
		assertTrue(d3.timeEnded().getHour()==0 && d3.timeEnded().getMinute()==0); 
		assertTrue(d3.pauseTime().getHour()==0 && d3.pauseTime().getMinute()==0); 
	}
	@Test
	public void timeWorkedNoPause() throws TimeStructureException {
		Time result = new Time(9,39);
		assertTrue(d1.timeWorked().getHour()==result.getHour() && d1.timeWorked().getMinute()==result.getMinute()); 
	}

	@Test
	public void timePayedNoPause() throws TimeStructureException {
		Time result = new Time(9,30);
		assertTrue(d1.timePayed().getHour() == result.getHour());
		assertTrue(d1.timePayed().getMinute() == 30); 
	}

	@Test
	public void pause() throws TimeStructureException {
		d1 = new Day(new Time(7,0), new Time(8,45));
		assertTrue(d1.pauseTime().getMinute() == 0);
		d1 = new Day(new Time(7,0), new Time(8,45), new Time(0,35));
		assertTrue(d1.pauseTime().getMinute() == 35); 
	}
	
	@Test
	public void payedPause() throws TimeStructureException {
		System.out.println(d2.payedPauseTime()+"\n"+d2.pauseTime());
		assertTrue(d2.payedPauseTime().getMinute() == 30); 
	}
	
	@Test
	public void timeWorkedWithPause() throws TimeStructureException {
		Time result = new Time(9,14);
		assertTrue(d2.timeWorked().getHour()==result.getHour() && d2.timeWorked().getMinute()==result.getMinute()); 
	}
	@Test
	public void timeBegun() throws TimeStructureException {
		Time result = new Time(7,9);
		assertTrue(d2.timeBegun().getHour()==result.getHour() && d2.timeBegun().getMinute()==result.getMinute()); 
	}
	@Test
	public void timeEnded() throws TimeStructureException {
		Time result = new Time(16,48);
		assertTrue(d2.timeEnded().getHour()==result.getHour() && d2.timeEnded().getMinute()==result.getMinute()); 
	}
	@Test
	public void timeBegunPayed() throws TimeStructureException {
		Time result = new Time(7,15);
		assertTrue(d2.timeBegunPayed().getHour()==result.getHour() && d2.timeBegunPayed().getMinute()==result.getMinute()); 
	}
	@Test
	public void timeEndedPayed() throws TimeStructureException {
		Time result = new Time(16,45);
		assertTrue(d2.timeEndedPayed().getHour()==result.getHour() && d2.timeEndedPayed().getMinute()==result.getMinute()); 
	}

	@Test
	public void timePayedWithPause() throws TimeStructureException {
		Time result = new Time(9,0);
		assertTrue(d2.timePayed().getHour() == result.getHour());
		assertTrue(d2.timePayed().getMinute() == result.getMinute()); 
	}

	@Test
	public void commentInitializedNotNull(){
		if(d1 != null){
			assertTrue(d1.getComment().equals(""));
		}
		else
			fail();
	}

	@Test
	public void commentSetAndGet(){
		d1.setComment("Test");
		assertTrue(d1.getComment().equals("Test"));
		d2.setComment("Hello");
		assertTrue(d2.getComment().equals("Hello"));


		assertTrue(d1.getComment().equals("Test"));
		d1.setComment("Testing");
		assertTrue(d1.getComment().equals("Testing"));
	}

	@Test
	public void scannedCardInitializedNotNull(){
		if(d1 != null){
			assertTrue(d1.getCheckInCardIsScanned() == true);
		}
		else
			fail();
	}

	@Test
	public void scannedCardSetAndGet(){
		d1.setCheckInCardIsScanned(false);
		assertTrue(d1.getCheckInCardIsScanned() == false);
		d2.setCheckInCardIsScanned(true);
		d2.setCheckInCardIsScanned(false);
		assertTrue(d1.getCheckInCardIsScanned() == false);
		assertTrue(d2.getCheckInCardIsScanned() == false);

		/*
		try{
			d2.setCheckInCardIsScanned((Boolean) null);
			fail();
			//Accepterar null
		}
		catch(NullPointerException e){
			assertTrue(true);
		}
		 */
	

	}
	@Test
	public void illnessStateAfterConstructor(){
		if(d1 != null){
			assertTrue(d1.getIllness() == false);
		}
		else
			fail();
	}
	@Test
	public void illnessSetAndGet(){
		d1.setIllness(true);
		assertTrue(d1.getIllness() == true);
		assertTrue(d2.getIllness() == false);
		d1.setIllness(false);		
		assertTrue(d1.getIllness() == false);

	}
}
