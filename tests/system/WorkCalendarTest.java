package system;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import exception.TimeStructureException;

import system.Day;
import system.WorkDate;
import system.Time;
import system.WorkCalendar;

public class WorkCalendarTest {
	WorkCalendar cal;
	WorkDate c;
	WorkDate b;
	Day z;
	Day x;
	Day y;
	
	@Before
	public void setUp() throws TimeStructureException{

		cal = new WorkCalendar();
		z = new Day(new Time(8,40), new Time(16,20));
		x = new Day(new Time(13,37), new Time(21,54));
		y = new Day(new Time(9,26), new Time(18,15));
	}
	@Test
	public void testPutOneValue() {
		c = new WorkDate(2011, 12, 20);
		assertTrue(!cal.containsKey(c));
		cal.put(c, z);
		assertTrue(cal.containsKey(c));
		
	}
	@Test
	public void testPutMoreValues() {
		c = new WorkDate(2001, 8, 4);
		assertTrue(!cal.containsKey(c));
		cal.put(c, z);
		assertTrue(cal.containsKey(c));
		
		
		c = new WorkDate(2011, 12, 20);
		assertTrue(!cal.containsKey(c));
		cal.put(c, y);
		assertTrue(cal.containsKey(c));
		
		
		
		c = new WorkDate(1992, 7, 11);
		assertTrue(!cal.containsKey(c));
		cal.put(c, x);
		assertTrue(cal.containsKey(c));
		
		//Recheck all with another instance "b"
		b = new WorkDate(2011, 12, 20);
		assertTrue(cal.containsKey(b));

		b = new WorkDate(1992, 7, 11);
		assertTrue(cal.containsKey(b));
		
		b = new WorkDate(2001, 8, 4);
		assertTrue(cal.containsKey(b));
		
		//False check
		b = new WorkDate(2005, 10, 25);
		assertTrue(!cal.containsKey(b));
	}
	@Test
	public void getTestOneValue(){
		testPutOneValue();
		c = new WorkDate(2011, 12, 20);
		assertTrue(cal.containsKey(c));
	}
	@Test
	public void getTestMoreValues(){
		testPutMoreValues();
		b = new WorkDate(2001, 8, 4);
		c = new WorkDate(1992, 7, 11);
		assertTrue(cal.containsKey(b)&&cal.containsKey(c));
		assertTrue(cal.get(c).toString().equals(x.toString()));
		assertTrue(cal.get(b).toString().equals(z.toString()));
		b = new WorkDate(2011, 12, 20);
		assertTrue(cal.get(b).toString().equals(y.toString()));
	}
	@Test
	public void removeCalEntries(){
		b = new WorkDate(1994, 11, 13);
		c = new WorkDate(1992, 7, 11);
		
		cal.put(b, x);
		cal.put(c, y);
		b = new WorkDate(1998, 8, 24);
		assertTrue(cal.containsKey(new WorkDate(1994,11,13))&&cal.containsKey(c)&&!cal.containsKey(b));
		//Testar borttagning
		assertTrue(cal.removeEntry(new WorkDate(1992,7,11)));
		assertTrue(cal.containsKey(new WorkDate(1994,11,13))&&!cal.containsKey(c)&&!cal.containsKey(b));
		assertTrue(!cal.removeEntry(new WorkDate(1996, 10, 26)));
	}
	@Test
	public void testSize(){
		assertEquals(cal.size(),0);
		c = new WorkDate(2011, 12, 20);
		cal.put(c, z);
		assertEquals(cal.size(),1);
		c = new WorkDate(2012, 07, 11);
		cal.put(c, z);
		assertEquals(cal.size(),2);
		// En dag d�r man inte jobbat ska inte r�knas!
		c = null;
		cal.put(c, z);
		assertEquals(cal.size(),2);
	}

}
