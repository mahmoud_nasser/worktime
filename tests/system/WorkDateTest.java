package system;

import static org.junit.Assert.*;

import java.util.GregorianCalendar;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;

import controllers.Settings;
import system.WorkDate;

public class WorkDateTest {
	WorkDate c;
	@Before
	public void setUp(){
		c = new WorkDate(2011, 12, 20);
	}

	@Test
	public void weekDay() {
		//The date is a Thursday, which is the 2:nd day of a week
		assertEquals(c.getDayOfWeek(), 2);
	}
	@Test
	public void gets() {

		assertEquals(c.getYear(), 2011);
		assertEquals(c.getMonth(), 12);
		assertEquals(c.getDay(), 20);
	}

	@Test
	public void returnCorrectHashCode(){
		assertEquals(c.hashCode(), 20111220);
	}
	
	@Test
	public void compareDates(){
		WorkDate b = new WorkDate(2011, 12, 20);
		assertTrue(c.compareTo(b) == 0);
		b = new WorkDate(2011, 12, 25);

		//Dag c �r L�NGRE FRAM i tiden �n b, dvs st�re �n noll
		assertTrue(c.compareTo(b)>0);
		b = new WorkDate(2011, 12, 15);
		//Dag c �r L�NGRE BAK i tiden �n b, dvs mindre �n noll
		assertTrue(c.compareTo(b)<0);
		b = new WorkDate(2012, 1, 5);
		//Dag c �r L�NGRE FRAM i tiden �n b, dvs st�rre �n noll
		assertTrue(c.compareTo(b)>0);		
	}
	
	@Test
	public void testSundays(){
		c = new WorkDate(2012,2,19);
		assertTrue(c.isSunday());
		c = new WorkDate(1992,07,12);
		assertTrue(c.isSunday());
		c = new WorkDate(2024,11,24);
		assertTrue(c.isSunday());
		c = new WorkDate(2024,11,25);
		assertTrue(!c.isSunday());
	}
	@Test
	public void testPrints(){
		GregorianCalendar g = new GregorianCalendar(2012,(3-1),25);
		System.out.println(g.getDisplayName(GregorianCalendar.MONTH, GregorianCalendar.LONG, new Locale("sv")));
		g = new GregorianCalendar(2012,(3-1),25);
		System.out.println("Int f�r m�nader\n" +GregorianCalendar.JANUARY+"\n"+GregorianCalendar.FEBRUARY 
				+"\n"+GregorianCalendar.DECEMBER);
		System.out.println("Danish  month begin payment\n"+WorkDate.monthPaymentBegin(2014, 6, Settings.PAYROLL_SYSTEM_DANISH));
		System.out.println("Swedish month begin payment\n"+WorkDate.monthPaymentBegin(2014, 6, Settings.PAYROLL_SYSTEM_SWEDISH));
		System.out.println("Danish  month end   payment\n"+WorkDate.monthPaymentEnd(2014, 6, Settings.PAYROLL_SYSTEM_DANISH));
		System.out.println("Swedish month end   payment\n"+WorkDate.monthPaymentEnd(2014, 6, Settings.PAYROLL_SYSTEM_SWEDISH));
		System.out.println("Danish  month end   payment\n"+WorkDate.monthPaymentEnd(2014, 3, Settings.PAYROLL_SYSTEM_DANISH));
		System.out.println("Swedish month end   payment\n"+WorkDate.monthPaymentEnd(2014, 3, Settings.PAYROLL_SYSTEM_SWEDISH));
		System.out.println("Int f�r Veckodagar\n"+GregorianCalendar.SUNDAY+"\n"+GregorianCalendar.MONDAY
				+"\n"+GregorianCalendar.TUESDAY+"\n"+GregorianCalendar.SATURDAY);
		System.out.println(c);
	}
	@Test
	public void increase(){
		//"Normal" date
		c = new WorkDate(2011, 12, 20);
		c = c.increaseDate();
		assertEquals(21, c.getDay());
		assertEquals(12, c.getMonth());
		assertEquals(2011, c.getYear());
		// New Month
		c = new WorkDate(2011, 3, 31);
		c = c.increaseDate();
		assertEquals(1, c.getDay());
		assertEquals(4, c.getMonth());
		assertEquals(2011, c.getYear());
		// New Year
		c = new WorkDate(2011, 12, 31);
		c = c.increaseDate();
		assertEquals(1, c.getDay());
		assertEquals(1, c.getMonth());
		assertEquals(2012, c.getYear());
		//February, non-Leap year
		c = new WorkDate(2011, 2, 28);
		c = c.increaseDate();
		assertEquals(1, c.getDay());
		assertEquals(3, c.getMonth());
		assertEquals(2011, c.getYear());
		//February, leapYear...
		c = new WorkDate(2012, 2, 28);
		c = c.increaseDate();
		assertEquals(29, c.getDay());
		assertEquals(2, c.getMonth());
		assertEquals(2012, c.getYear());
		//...and one more increase
		c = c.increaseDate();
		assertEquals(1, c.getDay());
		assertEquals(3, c.getMonth());
		assertEquals(2012, c.getYear());
	}
	
	
	
	@Test
	public void decrease(){
		//"Normal" date
		c = new WorkDate(2011, 12, 20);
		c = c.decreaseDate();
		assertEquals(19, c.getDay());
		assertEquals(12, c.getMonth());
		assertEquals(2011, c.getYear());
		// New Month
		c = new WorkDate(2011, 4, 1);
		c = c.decreaseDate();
		assertEquals(31, c.getDay());
		assertEquals(3, c.getMonth());
		assertEquals(2011, c.getYear());
		// New Year
		c = new WorkDate(2011, 1, 1);
		c = c.decreaseDate();
		assertEquals(31, c.getDay());
		assertEquals(12, c.getMonth());
		assertEquals(2010, c.getYear());
		//February, non-Leap year
		c = new WorkDate(2011, 3, 1);
		c = c.decreaseDate();
		assertEquals(28, c.getDay());
		assertEquals(2, c.getMonth());
		assertEquals(2011, c.getYear());
		//February, leapYear...
		c = new WorkDate(2012, 3, 1);
		c = c.decreaseDate();
		assertEquals(29, c.getDay());
		assertEquals(2, c.getMonth());
		assertEquals(2012, c.getYear());
		//...and one more decrease
		c = c.decreaseDate();
		assertEquals(28, c.getDay());
		assertEquals(2, c.getMonth());
		assertEquals(2012, c.getYear());
	}
	
	
}
