package system;

import static org.junit.Assert.*;

import org.junit.Test;

import exception.TimeStructureException;

import system.Time;


public class TimeTest {

	@Test
	public void midnightVariable(){
		assertTrue(Time.MIDNIGHT.getHour()==0 && Time.MIDNIGHT.getMinute()==0);
	}
	@Test
	public void timeDifference() throws TimeStructureException {
		Time start = new Time(15,15);
		Time end = new Time(13,45);
		assertTrue(start.timeDifference(end).getHour()==1);
		assertTrue(start.timeDifference(end).getMinute()==30);
		assertTrue(end.timeDifference(start).getHour()==1);
		assertTrue(end.timeDifference(start).getMinute()==30);
	}
	@Test
	public void timeDifferenceZerro() throws TimeStructureException {
		Time start = new Time(0,0);
		Time end = new Time(13,45);
		assertTrue(start.timeDifference(end).getHour()==13);
		assertTrue(start.timeDifference(end).getMinute()==45);
		assertTrue(end.timeDifference(start).getHour()==13);
		assertTrue(end.timeDifference(start).getMinute()==45);
	}

	@Test
	public void timeFormatException(){
		try{
			new Time(24,45);
			fail();
		}
		catch(TimeStructureException e){

		}
		try{
			new Time(13,60);
			fail();
		}
		catch(TimeStructureException e){

		}
		try{
			new Time(13,37);
		}
		catch(TimeStructureException e){
			fail();
		}
		assertTrue(true);
	}
	@Test
	public void changeTime() throws TimeStructureException {
		Time t1 = new Time(15,15);
		t1.setHour(14);
		t1.setMinute(43);
		assertTrue(t1.getHour()==14);
		assertTrue(t1.getMinute()==43);

	}
	@Test
	public void changeTimeIncorrectly() {
		Time t1;
		try{

			t1 = new Time(15,15);
			t1.setHour(24);
			fail();
		}
		catch(TimeStructureException e){
			assertTrue(true);
		}
		try{

			t1 = new Time(15,15);
			t1.setMinute(60);
			fail();
		}
		catch(TimeStructureException e){
			assertTrue(true);
		}

	}

	@Test
	public void printTime() throws TimeStructureException{
		Time t = new Time(13,37);
		assertTrue(t.toString().equals("13:37"));
		t = new Time(1,6);
		assertTrue(t.toString().equals("01:06"));
	}

}