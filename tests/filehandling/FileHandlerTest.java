package filehandling;

import static org.junit.Assert.*;

import org.junit.Test;

import filehandling.FileHandler;

public class FileHandlerTest {

	@Test
	public void SavingWithCustomFileName() {
		assertEquals(FileHandler.adjustFileNameExtension("MyCal.tja"), "MyCal.tja.mwc");
		assertEquals(FileHandler.adjustFileNameExtension("MyCal"), "MyCal.mwc");
		assertEquals(FileHandler.adjustFileNameExtension("MyCal."), "MyCal.mwc");
		assertEquals(FileHandler.adjustFileNameExtension("MyCal.mwc"), "MyCal.mwc");
		assertEquals(FileHandler.adjustFileNameExtension("MyCal.MwC"), "MyCal.MwC");
	}
	@Test
	public void CeckingFileNames() {
		assertFalse(FileHandler.fileHasCorrectExtension("MyCal.tja"));
		assertFalse(FileHandler.fileHasCorrectExtension("MyCal"));
		assertFalse(FileHandler.fileHasCorrectExtension("MyCal."));
		assertTrue(FileHandler.fileHasCorrectExtension("MyCal.mwc"));
		assertTrue(FileHandler.fileHasCorrectExtension("MyCal.MwC"));
	}

}
