package filehandling;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import org.junit.*;

import UI.MainWindow;
import system.Day;
import system.Time;
import system.WorkCalendar;
import system.WorkDate;
import exception.TimeStructureException;
import exception.XMLFileException;

public class XMLReaderTest {
	File xmlFile;
	WorkCalendar cal;

	@Before
	public void BuildUp() throws TimeStructureException{
		new MainWindow();
		cal = new WorkCalendar();
		Day d = new Day(new Time(8,40), new Time(16,20));
		d.setComment("My Birthday!");
		cal.put(new WorkDate(2014, 07, 11), d );
		d = new Day(new Time(8,27), new Time(18,22));
		d.setCheckInCardIsScanned(false);
		d.setComment("Forgot to scan card att the morning");
		cal.put(new WorkDate(2014, 7, 12), d);
		cal.put(new WorkDate(2014, 7, 13), d);
		cal.put(new WorkDate(2014, 7, 14), d);
		cal.put(new WorkDate(2014, 7, 22), d);
		cal.put(new WorkDate(2014, 8, 1), d);
		
		
		/*
		try {
			xmlFile = new File("testfile.cfg");
			//xmlFile = File.createTempFile("testfile", "xml");
			//file.deleteOnExit();
			PrintWriter writer = new PrintWriter(xmlFile, "UTF-8");
			writer.println("<WorkCalendar>");
			
			writer.println("\t<WorkDayEntry>");
			writer.println("\t\t<Date>01-05-2014</Date>");
			writer.println("\t\t<BeginStamp>07:15</BeginStamp>");
			writer.println("\t\t<EndStamp>07:15</EndStamp>");
			writer.println("\t\t<PauseLength>00:30</PauseLength>");
			writer.println("\t\t<IllnessFlag>FALSE</IllnessFlag>");
			writer.println("\t\t<Remarks>Hej hopp tjenare 123 ! osv</Remarks>");
			writer.println("\t</WorkDayEntry>");
			
			writer.println("\t<WorkDayEntry>");
			writer.println("\t\t<Date>02-05-2014</Date>");
			writer.println("\t\t<BeginStamp>07:15</BeginStamp>");
			writer.println("\t\t<EndStamp>07:15</EndStamp>");
			writer.println("\t\t<PauseLength>00:30</PauseLength>");
			writer.println("\t\t<IllnessFlag>FALSE</IllnessFlag>");
			writer.println("\t\t<Remarks>Nummer tv�</Remarks>");
			writer.println("\t</WorkDayEntry>");
			
			writer.println("</WorkCalendar>");
			writer.close();
			
		} catch (IOException e) {
			fail("TestError");
			e.printStackTrace();
		}
		*/
	}
	
	@Test
	public void saveValues() throws XMLFileException, FileNotFoundException, IOException {
		//FileHandler.xmlsaveToFile(new File("test.wmc"));
		assertTrue(true);
		//assertEquals(read.getTagValue("CalendarEntry", "Remarks"),"Hej hopp tjenare 123 ! osv");
	
		
	}
	
	/*
	@Test
	public void ReadValues() throws XMLFileException {
		XMLReader read = new XMLReader(xmlFile);

		System.out.println(read.getTagValue("WorkCalendar","WorkDayEntry", "Remarks"));
		assertTrue(true);
		//assertEquals(read.getTagValue("CalendarEntry", "Remarks"),"Hej hopp tjenare 123 ! osv");		
	}
*/
	
}
